package com.menze.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

@EnableResourceServer
@Configuration
public class ResourceServer extends ResourceServerConfigurerAdapter {

    private final DefaultTokenServices tokenServices;

    @Autowired
    public ResourceServer(DefaultTokenServices tokenServices) {
        this.tokenServices = tokenServices;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers(HttpMethod.OPTIONS).permitAll()
                .and().authorizeRequests().antMatchers("/user", "/defaultCity", "/comment", "/rating", "/menzameal/*/myrating", "/messages", "/message/*/read").hasAnyAuthority("ADMIN", "USER")
                .and().authorizeRequests().antMatchers(HttpMethod.POST, "/picture").hasAnyAuthority("ADMIN", "USER")
                .and().authorizeRequests().antMatchers("/menza/*/dailyoffer/update", "/picture/approve/*/*", "/message/*").hasAnyAuthority("ADMIN")
                .and().authorizeRequests().antMatchers(HttpMethod.GET, "/picture").hasAnyAuthority("ADMIN")
                .and().authorizeRequests().anyRequest().permitAll();
//        http.
//                authorizeRequests()
//                .antMatchers(ENDPOINTS).permitAll() //open endpoints
//                .and().authorizeRequests().anyRequest().authenticated();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer config) {
        config.tokenServices(tokenServices);
    }
}
