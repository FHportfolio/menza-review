package com.menze.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MENZA")
@Data
public class Menza implements Serializable {

    private static final long serialVersionUID = -7296130738770071584L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long ID;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CITY_ID")
    private City city;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "latitude", nullable = false)
    private double latitude;

    @Column(name = "longitude", nullable = false)
    private double longitude;

    @Column(name = "script", nullable = false)
    private String scriptLocation;

    @Column(name = "info", nullable = false)
    private String info;
}

