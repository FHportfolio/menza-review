package com.menze.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "MESSAGE")
@Data
public class Message implements Serializable {

    private static final Long serialVersionUID = -1145292439787579011L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long ID;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    @Column(name = "timestamp", nullable = false)
    private Timestamp timestamp;

    @Column(name = "text", nullable = false)
    private String text;

    @Column(name = "read", nullable = false)
    private boolean read;

}
