package com.menze.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CITY")
@Data
public class City implements Serializable {

    private static final long serialVersionUID = -5519669561259793164L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long ID;

    @Column(name = "name", nullable = false)
    private String name;

}
