package com.menze.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "USERS")
@Data
@NoArgsConstructor
public class User implements Serializable {

    private static final long serialVersionUID = 7016007193014717433L;

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator",
            parameters = {
                    @Parameter(
                            name = "uuid_gen_strategy_class",
                            value = "org.hibernate.id.uuid.CustomVersionOneStrategy"
                    )
            }
    )
    @Column(name = "ID", updatable = false, nullable = false)
    private UUID ID;

    @Column(name = "username", updatable = false, nullable = false, unique = true)
    private String username;

    @Column(name = "email", unique = true, updatable = false, nullable = false)
    private String email;

    @Column(name = "password_hash", updatable = false, nullable = false)
    private String passwordHash;

    @ManyToOne
    @JoinColumn(name = "CITY_ID")
    private City city;

    @Column(name = "admin", nullable = false)
    private boolean admin;
}
