package com.menze.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MENZA_MEAL")
@Data
public class MenzaMeal implements Serializable {

    private static final long serialVersionUID = -5070155534282976645L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long ID;

    @ManyToOne
    @JoinColumn(name = "MENZA_ID")
    private Menza menza;

    @ManyToOne
    @JoinColumn(name = "MEAL_ID")
    private Meal meal;
}
