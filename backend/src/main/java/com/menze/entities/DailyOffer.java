package com.menze.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "DAILY_OFFER")
@Data
public class DailyOffer implements Serializable {

    private static final long serialVersionUID = 1869410970183449409L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long ID;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "MENZA_MEAL_ID")
    private MenzaMeal menzaMeal;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MENZA_ID")
    private Menza menza;

    @Column(name = "time_type", nullable = false)
    private String timeType; //dorucak rucak

    @Column(name = "offer_type", nullable = false)
    private String offerType; //meni izbor

    @Column(name = "line", nullable = false)
    private String line; //linija 1 linija 2
}
