package com.menze.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "PICTURE")
@Data
public class Picture implements Serializable {

    private static final long serialVersionUID = -1990697273533788513L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    @ManyToOne
    @JoinColumn(name = "MENZA_MEAL_ID")
    private MenzaMeal menzaMeal;

    @Column(name = "description")
    private String description;

    @Column(name = "image", nullable = false)
    @Lob
    private byte[] image;

    @Column(name = "timestamp", nullable = false)
    private Timestamp timestamp;

    @Column(name = "approved", nullable = false)
    private boolean approved;
}
