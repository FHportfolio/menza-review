package com.menze.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MEAL")
@Data
public class Meal implements Serializable {

    private static final long serialVersionUID = 7534294343540032066L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long ID;

    @Column(name = "name", nullable = false)
    private String name;
}
