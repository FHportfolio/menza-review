package com.menze.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;

@Entity
@Table(name = "RATING")
@Data
public class Rating implements Serializable {

    private static final long serialVersionUID = -1990697273533788513L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long ID;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    @ManyToOne
    private MenzaMeal menzaMeal;

    @Column(name = "rating")
    @Min(0)
    @Max(5)
    private int rating;
}
