package com.menze.repositories;

import com.menze.entities.MenzaMeal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MenzaMealRepository extends JpaRepository<MenzaMeal, Long> {

    Optional<MenzaMeal> findFirstByMealNameAndMenzaID(String mealName, Long menzaId);

    List<MenzaMeal> findAllByMealNameAndMenzaID(String mealName, Long menzaId);
    boolean existsByMealName(String mealName);
}
