package com.menze.repositories;

import com.menze.entities.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {

    @Query("SELECT AVG(r.rating) from Rating r where menza_meal_id=:menzaMealId")
    Optional<Double> getAverageRatingByMenzaMealId(@Param("menzaMealId") Long menzaMealId);

    boolean existsByMenzaMealIDAndUserID(Long menzaMealId, UUID userId);

    void deleteByUserIDAndMenzaMealID(UUID user_ID, Long menzaMeal_ID);

    Optional<Rating> findByUserIDAndMenzaMealID(UUID userId, Long menzaMealId);



}
