package com.menze.repositories;

import com.menze.entities.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {

    List<Message> findAllByUserEmailOrderByTimestampDesc(String email);

    Page<Message> findAllByUserEmailOrderByTimestampDesc(String email, Pageable pageable);

}
