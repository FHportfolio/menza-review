package com.menze.repositories;

import com.menze.entities.Menza;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface MenzaRepository extends JpaRepository<Menza, Long> {
    List<Menza> findAllByCityID(Long cityId);
}
