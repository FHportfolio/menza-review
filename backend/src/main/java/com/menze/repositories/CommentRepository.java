package com.menze.repositories;

import com.menze.entities.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findAllByMenzaMealID(Long menzaMealId);

    Page<Comment> findAllByMenzaMealIDOrderByTimestampDesc(Long menzaMealId, Pageable pageable);

}
