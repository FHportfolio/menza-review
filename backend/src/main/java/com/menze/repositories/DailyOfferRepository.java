package com.menze.repositories;

import com.menze.entities.DailyOffer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DailyOfferRepository extends JpaRepository<DailyOffer, Long> {
    List<DailyOffer> findAllByMenzaID(long menzaId);

    void removeAllByMenzaID(long menzaId);
}
