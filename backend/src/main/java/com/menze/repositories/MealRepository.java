package com.menze.repositories;

import com.menze.entities.Meal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MealRepository extends JpaRepository<Meal, Long> {

    boolean existsByName(String name);

    Optional<Meal> findByName(String name);
}
