package com.menze.repositories;

import com.menze.entities.Picture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PictureRepository extends JpaRepository<Picture, Long> {

    List<Picture> findAllByMenzaMealID(Long menzaMealId);

    List<Picture> findAllByApproved(boolean approved);

    List<Picture> findAllByMenzaMealIDAndApproved(Long menzaMealId, Boolean approved);
    void removeById(Long id);
//    void deleteById(Long ID);
}
