package com.menze.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class RatingDTO {

    private Long id;

    private UUID userId;

    private Long menzaMealId;

    private int rating;

}
