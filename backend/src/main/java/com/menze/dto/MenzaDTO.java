package com.menze.dto;

import lombok.Data;

@Data
public class MenzaDTO {

    private Long ID;

    private Long CityID;

    private String name;

    private double latitude;

    private double longitude;

    private String scriptLocation;

    private String info;
}
