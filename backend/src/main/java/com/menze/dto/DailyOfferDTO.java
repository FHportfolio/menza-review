package com.menze.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class DailyOfferDTO implements Serializable {

    private Long ID;

    private Long menzaMealId;

    private String name;

    private Long menzaId;

    private String timeType;

    private String offerType;

    private String line;
}
