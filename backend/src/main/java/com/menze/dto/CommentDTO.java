package com.menze.dto;

import lombok.Data;

import java.sql.Timestamp;
import java.util.UUID;

@Data
public class CommentDTO {

    private Long id;

    private UUID userId;

    private String username;

    private Long menzaMealId;

    private String text;

    private Timestamp timestamp;
}
