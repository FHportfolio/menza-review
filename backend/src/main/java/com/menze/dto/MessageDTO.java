package com.menze.dto;

import lombok.Data;

import java.sql.Timestamp;
import java.util.UUID;

@Data
public class MessageDTO {

    private Long id;

    private UUID userId;

    private Timestamp timestamp;

    private String text;

    private boolean read;
}
