package com.menze.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class UserDTO {

    private UUID ID;

    private String username;

    private String email;

    private Long cityId;

    private String password;

    private boolean admin;
}
