package com.menze.dto;

import lombok.Data;

import java.sql.Timestamp;
import java.util.UUID;

@Data
public class PictureDTO {

    private Long id;

    private UUID userId;

    private String username;

    private Long menzaMealId;

    private String description;

    private byte[] image;

    private Timestamp timestamp;

    private boolean approved;

}
