package com.menze.dto.mappers.impl;

import com.menze.dto.DailyOfferDTO;
import com.menze.dto.mappers.Mapper;
import com.menze.entities.DailyOffer;
import com.menze.repositories.MenzaMealRepository;
import com.menze.repositories.MenzaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DailyOfferMapper implements Mapper<DailyOffer, DailyOfferDTO> {

    private final MenzaMealRepository menzaMealRepository;
    private final MenzaRepository menzaRepository;

    @Autowired
    public DailyOfferMapper(MenzaMealRepository menzaMealRepository, MenzaRepository menzaRepository) {
        this.menzaMealRepository = menzaMealRepository;
        this.menzaRepository = menzaRepository;
    }

    @Override
    public DailyOffer dtoToEntity(DailyOfferDTO dto) {
        DailyOffer entity = new DailyOffer();
        entity.setID(dto.getID());
        entity.setMenzaMeal(menzaMealRepository.findById(dto.getMenzaMealId()).orElse(null));
        entity.setMenza(menzaRepository.findById(dto.getMenzaId()).orElse(null));
        entity.setTimeType(dto.getTimeType());
        entity.setOfferType(dto.getOfferType());
        entity.setLine(dto.getLine());
        return entity;
    }

    @Override
    public DailyOfferDTO entityToDto(DailyOffer entity) {
        DailyOfferDTO dto = new DailyOfferDTO();
        dto.setID(entity.getID());
        dto.setMenzaMealId(entity.getMenzaMeal().getID());
        dto.setName(entity.getMenzaMeal().getMeal().getName());
        dto.setMenzaId(entity.getMenza().getID());
        dto.setTimeType(entity.getTimeType());
        dto.setOfferType(entity.getOfferType());
        dto.setLine(entity.getLine());
        return dto;
    }
}
