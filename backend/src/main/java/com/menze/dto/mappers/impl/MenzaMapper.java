package com.menze.dto.mappers.impl;

import com.menze.dto.MenzaDTO;
import com.menze.dto.mappers.Mapper;
import com.menze.entities.Menza;
import com.menze.repositories.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MenzaMapper implements Mapper<Menza, MenzaDTO> {

    private final CityRepository cityRepository;

    @Autowired
    public MenzaMapper(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public Menza dtoToEntity(MenzaDTO dto) {
        Menza entity = new Menza();

        entity.setID(dto.getID());
        entity.setCity(cityRepository.findById(dto.getCityID()).orElse(null));
        entity.setInfo(dto.getInfo());
        entity.setLatitude(dto.getLatitude());
        entity.setLongitude(dto.getLongitude());
        entity.setName(dto.getName());
        entity.setScriptLocation(dto.getScriptLocation());

        return entity;
    }

    @Override
    public MenzaDTO entityToDto(Menza entity) {
        MenzaDTO dto = new MenzaDTO();

        dto.setCityID(entity.getCity().getID());
        dto.setID(entity.getID());
        dto.setInfo(entity.getInfo());
        dto.setLatitude(entity.getLatitude());
        dto.setLongitude(entity.getLongitude());
        dto.setName(entity.getName());
        dto.setScriptLocation(entity.getScriptLocation());

        return dto;
    }
}
