package com.menze.dto.mappers.impl;

import com.menze.dto.MessageDTO;
import com.menze.dto.mappers.Mapper;
import com.menze.entities.Message;
import com.menze.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageMapper implements Mapper<Message, MessageDTO> {


    private final UserRepository userRepository;

    @Autowired
    public MessageMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Message dtoToEntity(MessageDTO dto) {
        Message entity = new Message();

        entity.setID(dto.getId());
        entity.setUser(userRepository.findById(dto.getUserId()).orElse(null));
        entity.setTimestamp(dto.getTimestamp());
        entity.setText(dto.getText());
        entity.setRead(dto.isRead());

        return entity;
    }

    @Override
    public MessageDTO entityToDto(Message entity) {
        MessageDTO dto = new MessageDTO();

        dto.setId(entity.getID());
        dto.setUserId(entity.getUser().getID());
        dto.setTimestamp(entity.getTimestamp());
        dto.setText(entity.getText());
        dto.setRead(entity.isRead());

        return dto;
    }
}
