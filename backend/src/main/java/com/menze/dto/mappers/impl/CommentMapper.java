package com.menze.dto.mappers.impl;

import com.menze.dto.CommentDTO;
import com.menze.dto.mappers.Mapper;
import com.menze.entities.Comment;
import com.menze.repositories.MenzaMealRepository;
import com.menze.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommentMapper implements Mapper<Comment, CommentDTO> {

    private final UserRepository userRepository;
    private final MenzaMealRepository menzaMealRepository;

    @Autowired
    public CommentMapper(UserRepository userRepository, MenzaMealRepository menzaMealRepository) {
        this.userRepository = userRepository;
        this.menzaMealRepository = menzaMealRepository;
    }

    @Override
    public Comment dtoToEntity(CommentDTO dto) {
        Comment entity = new Comment();

        entity.setUser(userRepository.findById(dto.getUserId()).orElse(null));
        entity.setMenzaMeal(menzaMealRepository.findById(dto.getMenzaMealId()).orElse(null));
        entity.setText(dto.getText());
        entity.setTimestamp(dto.getTimestamp());

        return entity;
    }

    @Override
    public CommentDTO entityToDto(Comment entity) {
        CommentDTO commentDTO = new CommentDTO();

        commentDTO.setId(entity.getID());
        commentDTO.setUserId(entity.getUser().getID());
        commentDTO.setUsername(entity.getUser().getUsername());
        commentDTO.setMenzaMealId(entity.getMenzaMeal().getID());
        commentDTO.setTimestamp(entity.getTimestamp());
        commentDTO.setText(entity.getText());

        return commentDTO;
    }
}
