package com.menze.dto.mappers.impl;

import com.menze.dto.UserDTO;
import com.menze.dto.mappers.Mapper;
import com.menze.entities.User;
import com.menze.repositories.CityRepository;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserMapper implements Mapper<User, UserDTO> {

    private final CityRepository cityRepository;
    private final PasswordEncoder passwordEncoder;

    public UserMapper(CityRepository cityRepository, @Lazy PasswordEncoder passwordEncoder1) {
        this.cityRepository = cityRepository;

        this.passwordEncoder = passwordEncoder1;
    }

    @Override
    public User dtoToEntity(UserDTO dto) {
        User entity = new User();

        entity.setUsername(dto.getUsername());
        entity.setEmail(dto.getEmail());
        entity.setCity(cityRepository.findById(dto.getCityId()).orElse(null));
        entity.setPasswordHash(passwordEncoder.encode(dto.getPassword()));
        entity.setAdmin(dto.isAdmin());

        return entity;
    }

    @Override
    public UserDTO entityToDto(User entity) {
        UserDTO dto = new UserDTO();

        dto.setID(entity.getID());
        dto.setUsername(entity.getUsername());
        dto.setEmail(entity.getEmail());
        dto.setCityId(entity.getCity().getID());
        dto.setAdmin(entity.isAdmin());

        return dto;
    }
}
