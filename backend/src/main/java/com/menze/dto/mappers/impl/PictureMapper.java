package com.menze.dto.mappers.impl;

import com.menze.dto.PictureDTO;
import com.menze.dto.mappers.Mapper;
import com.menze.entities.Picture;
import com.menze.repositories.MenzaMealRepository;
import com.menze.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class PictureMapper implements Mapper<Picture, PictureDTO> {

    private final UserRepository userRepository;
    private final MenzaMealRepository menzaMealRepository;

    @Autowired
    public PictureMapper(UserRepository userRepository, MenzaMealRepository menzaMealRepository) {
        this.userRepository = userRepository;
        this.menzaMealRepository = menzaMealRepository;
    }

    @Override
    public Picture dtoToEntity(PictureDTO dto) {

        Picture entity = new Picture();
        if (dto.getId() != null) {
            entity.setId(dto.getId());
        }
        entity.setUser(userRepository.findById(dto.getUserId()).orElse(null));
        entity.setMenzaMeal(menzaMealRepository.findById(dto.getMenzaMealId()).orElse(null));
        entity.setDescription(dto.getDescription());
        entity.setImage(dto.getImage());
        entity.setTimestamp(dto.getTimestamp());
        entity.setApproved(dto.isApproved());

        return entity;
    }

    @Override
    public PictureDTO entityToDto(Picture entity) {

        PictureDTO pictureDTO = new PictureDTO();

        pictureDTO.setId(entity.getId());
        pictureDTO.setUserId(entity.getUser().getID());
        pictureDTO.setUsername(entity.getUser().getUsername());
        pictureDTO.setMenzaMealId(entity.getMenzaMeal().getID());
        pictureDTO.setDescription(entity.getDescription());
        pictureDTO.setImage(null);
        pictureDTO.setTimestamp(entity.getTimestamp());
        pictureDTO.setApproved(entity.isApproved());

        return pictureDTO;
    }
}
