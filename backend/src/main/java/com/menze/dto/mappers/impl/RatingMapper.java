package com.menze.dto.mappers.impl;

import com.menze.dto.RatingDTO;
import com.menze.dto.mappers.Mapper;
import com.menze.entities.Rating;
import com.menze.repositories.MenzaMealRepository;
import com.menze.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RatingMapper implements Mapper<Rating, RatingDTO> {

    private final UserRepository userRepository;
    private final MenzaMealRepository menzaMealRepository;

    @Autowired
    public RatingMapper(UserRepository userRepository, MenzaMealRepository menzaMealRepository) {
        this.userRepository = userRepository;
        this.menzaMealRepository = menzaMealRepository;
    }

    @Override
    public Rating dtoToEntity(RatingDTO dto) {

        Rating entity = new Rating();

        if (dto.getId() != null) {
            entity.setID(dto.getId());
        }
        entity.setUser(userRepository.findById(dto.getUserId()).orElse(null));
        entity.setMenzaMeal(menzaMealRepository.findById(dto.getMenzaMealId()).orElse(null));
        entity.setRating(dto.getRating());

        return entity;
    }

    @Override
    public RatingDTO entityToDto(Rating entity) {

        RatingDTO ratingDTO = new RatingDTO();

        ratingDTO.setId(entity.getID());
        ratingDTO.setUserId(entity.getUser().getID());
        ratingDTO.setMenzaMealId(entity.getMenzaMeal().getID());
        ratingDTO.setRating(entity.getRating());

        return ratingDTO;
    }
}
