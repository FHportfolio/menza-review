package com.menze.dto;

import lombok.Data;

@Data
public class MealDTO {

    private String timeType;

    private String line;

    private String offerType;

    private String name;

    @Override
    public String toString() {
        return "MealDTO{" +
                "timeType='" + timeType + '\'' +
                ", line='" + line + '\'' +
                ", offerType='" + offerType + '\'' +
                ", name='" + name + '\'' +
                "}\n";
    }
}
