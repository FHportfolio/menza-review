package com.menze.controllers;

import com.menze.services.MenzaMealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.http.ResponseEntity.ok;


@Controller
public class MenzaMealController {

    private final MenzaMealService menzaMealService;

    @Autowired
    public MenzaMealController(MenzaMealService menzaMealService) {
        this.menzaMealService = menzaMealService;
    }


    @GetMapping("/menzameal")
    public ResponseEntity getMenzaMeal(@RequestParam("id") Long id) {
        return ok(menzaMealService.getMenzaMealById(id));
    }
}
