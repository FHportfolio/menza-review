package com.menze.controllers;

import com.menze.dto.RatingDTO;
import com.menze.services.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController

public class RatingController {
    private final RatingService ratingService;

    @Autowired
    public RatingController(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    @GetMapping("/menzameal/{menzaMealId}/rating")
    public ResponseEntity getAverageRatingByMenzaMealId(@PathVariable(value = "menzaMealId") Long menzaMealId) {
        return ResponseEntity.ok(ratingService.getAverageRatingByMenzaMealId(menzaMealId));
    }

    @PostMapping("/rating")
    public ResponseEntity savingRating(@RequestBody RatingDTO dto) {
        ratingService.saveRating(dto);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/menzameal/{menzaMealId}/myrating")
    public ResponseEntity getMyRatingByMenzaMealId(@PathVariable("menzaMealId") Long menzaMealId) {
        return ResponseEntity.ok(ratingService.getMyRating(menzaMealId));
    }

}