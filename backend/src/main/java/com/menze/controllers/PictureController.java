package com.menze.controllers;

import com.menze.dto.PictureDTO;
import com.menze.services.PictureService;
import com.menze.services.impl.PictureServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;


@RestController
public class PictureController {

    private final PictureService pictureService;

    @Autowired
    public PictureController(PictureServiceImpl pictureService) {
        this.pictureService = pictureService;
    }

    @PostMapping("/picture/approve/{id}/{approved}")
    public ResponseEntity approve(@PathVariable(value = "approved") Boolean approved,@PathVariable(value = "id") Long pictureId) {
        try {
            if (approved) pictureService.approve(pictureId);
            else pictureService.deletePicture(pictureId);
        }catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/img/src/{pictureId}", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getPictureById(@PathVariable(value = "pictureId") Long id) {
        byte[] image = pictureService.getImageById(id);
        return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(image);
    }

    @GetMapping(value = "/menzameal/{menzaMealId}/images")
    public ResponseEntity<List<PictureDTO>> getAllPictureByMenzaMealId(@PathVariable(value = "menzaMealId") Long menzaMealId){
        return ResponseEntity.ok(pictureService.getAllPictureByMenzaMealId(menzaMealId));
    }


    @PostMapping("/picture")
    public ResponseEntity saveImage(@RequestBody PictureDTO pictureDTO) {
        pictureService.savePicture(pictureDTO);
        return ResponseEntity.ok("kao je to to");
    }

    @GetMapping("/picture")
    public ResponseEntity getUnApprovedImages(){
        return ResponseEntity.ok(pictureService.findAllPictureByApproved(false));
    }
}
