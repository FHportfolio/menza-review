package com.menze.controllers;

import com.menze.dto.CommentDTO;
import com.menze.services.CommentService;
import com.menze.services.impl.CommentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
public class CommentController {

    private final CommentService commentService;

    @Autowired
    public CommentController(CommentServiceImpl commentService) {
        this.commentService = commentService;
    }

    @GetMapping("/menzameal/{menzaMealId}/comment/page/{pageno}")
    public ResponseEntity<List<CommentDTO>> getCommentsByMenzaMealPageable(@PathVariable Long menzaMealId, @PathVariable("pageno") int pageNum, @RequestParam(value = "size", defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(commentService.getCommentByMenzaMealIdPageable(menzaMealId, PageRequest.of(pageNum, pageSize)));
    }

    @GetMapping("/menzameal/{menzaMealId}/comments")
    public ResponseEntity<List<CommentDTO>> getAllCommentByMenzaMeal(@PathVariable(value = "menzaMealId") Long menzaMealId) {
        return ResponseEntity.ok(commentService.getAllCommentByMenzaMealId(menzaMealId));
    }

    @DeleteMapping("/comment")
    public ResponseEntity<Void> deleteCommmentById(@RequestParam(value = "id") Long commentId) {
        try{
            commentService.deleteCommentById(commentId);
        }
        catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        return ResponseEntity.ok().build();
    }

    @PostMapping("/comment")
    public ResponseEntity<Void> saveComment(@RequestBody CommentDTO dto) {
        try {
            commentService.saveComment(dto);
        } catch(IllegalArgumentException e) {
            ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        }
        return ResponseEntity.ok().build();
    }
}
