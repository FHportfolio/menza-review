package com.menze.controllers;

import com.menze.dto.MessageDTO;
import com.menze.dto.UserDTO;
import com.menze.services.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@RestController
public class UserController {

    private final UserServiceImpl userService;

    @Autowired
    public UserController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @GetMapping("/messages")
    public ResponseEntity<List<MessageDTO>> getMessages() {
        List<MessageDTO> messageDTOList = userService.getAllMessageDtoByEmail(userService.getCurrentUser().getEmail());
        messageDTOList.sort(Comparator.comparing(MessageDTO::getTimestamp));
        Collections.reverse(messageDTOList);
        return ResponseEntity.ok(messageDTOList);
    }

    @PostMapping("/message/{messageId}/read")
    public ResponseEntity<Void> readMessage(@PathVariable(value = "messageId") Long messageId) {
        userService.readMessage(messageId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/messages/page/{pageno}")
    public ResponseEntity<List<MessageDTO>> getMessagesByMenzaMealPageable(@PathVariable("pageno") int pageNum, @RequestParam(value = "size", defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(userService.getAllMessageDtoByEmailPageable(PageRequest.of(pageNum, pageSize)));
    }

    @PostMapping("/message/{imageId}")
    public ResponseEntity sendMessage(@RequestBody MessageDTO messageDTO, @PathVariable(value = "imageId") Long imageId) {
        userService.sendMessage(messageDTO, imageId);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/defaultCity")
    public ResponseEntity setDefaultCity(@RequestParam(value = "id") Long id) {
        if (userService.changeDefaultCity(id)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/user")
    public UserDTO getUserInfo() {
        return userService.getCurrentUserDTO();
    }

    @PostMapping("/register")
    public ResponseEntity savingUser(@RequestBody UserDTO dto) {
        List<String> errors = new ArrayList<>();
        if (userService.userExistsWithUsername(dto.getUsername())) {
            errors.add("Username already exists.");
        }
        if (userService.userExistsWithEmail(dto.getEmail())){
            errors.add("Email already exists.");
        }
        if (errors.size() != 0){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(errors);
        }
        userService.saveUser(dto);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/messages/unread")
    public ResponseEntity getNumberOfUnreadMessages() {
        return ResponseEntity.ok(userService.getAllMessageDtoByEmail(userService.getCurrentUser().getEmail()).stream().filter(messageDTO -> !messageDTO.isRead()).count());
    }
}
