package com.menze.controllers;

import com.menze.dto.DailyOfferDTO;
import com.menze.services.DailyOfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DailyOfferController {

    private final DailyOfferService dailyOfferService;

    @Autowired
    public DailyOfferController(DailyOfferService dailyOfferService) {
        this.dailyOfferService = dailyOfferService;
    }

    @GetMapping("/menza/{menzaId}/dailyoffer")
    public ResponseEntity<List<DailyOfferDTO>> getDailyOfferByMenzaID(@PathVariable(value = "menzaId") Long menzaId) {
        return ResponseEntity.ok(dailyOfferService.gelAllByMenzaID(menzaId));
    }

    @GetMapping("/menza/{menzaId}/dailyoffer/update")
    public ResponseEntity<Void> updateDailyOfferByMenzaId(@PathVariable(value = "menzaId") Long menzaId) {
        dailyOfferService.updateMenu(menzaId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/menza/dailyoffer/update")
    public ResponseEntity<Void> updateAlldailyOffers() {
        dailyOfferService.updateAllMenu();
        return ResponseEntity.ok().build();
    }
}
