package com.menze.controllers;

import com.menze.dto.MenzaDTO;
import com.menze.services.impl.MenzaServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
public class MenzaController {
    //TODO Error handling needed ako netko stigne//

    private final MenzaServiceImpl menzaService;

    @Autowired
    public MenzaController(MenzaServiceImpl menzaService) {
        this.menzaService = menzaService;
    }

    @GetMapping("/city/{cityId}/menze")
    public ResponseEntity<List<MenzaDTO>> getAllMenzaByCityId(@PathVariable("cityId") Long cityId) {
        return ok(menzaService.getAllMenzaDtoByCityId(cityId));
    }

    @GetMapping("/menza")
    public ResponseEntity getMenzaById(@RequestParam("id") Long id) {
        return ok(menzaService.getMenzaByID(id));
    }

}
