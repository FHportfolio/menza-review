import requests
from bs4 import BeautifulSoup

url = "http://www.sczg.unizg.hr/prehrana/restorani/sd-s-radic/"
response = requests.get(url)
html = response.content

soup = BeautifulSoup(html, "lxml")
nonBreakSpace = u'\xa0'
p = soup.findAll(['p'])

ignoreFlag = True

currentMeal = "Ručak"
currentMenu = ''
currentLine = ''

final = dict()

for i in p[:len(p) - 2]:
    pureText = i.get_text().replace(nonBreakSpace, "")
    pureText = ' '.join(pureText.split())
    pureText = pureText.strip()
    if pureText == '':
        continue
    if pureText.startswith("RESTORAN 1"):
        ignoreFlag = False
        currentLine = "linija 1"
        final[currentMeal] = dict()
        continue
    if ignoreFlag == False:
        # print(pureText)
        if pureText.startswith('RESTORAN 2'):
            currentLine = 'linija 2'
            continue
        if pureText.startswith("RUCAK"):
            currentMeal = 'Ručak'
            continue
        if pureText.startswith("VECERA"):
            currentMeal = 'Večera'
            continue
        if pureText.startswith('MENU'):
            currentMenu = 'menu'
            pureText = pureText.split(':')[1]
            if currentMeal not in final:
                final[currentMeal] = dict()
            if currentLine not in final[currentMeal]:
                final[currentMeal][currentLine] = dict()
            if currentMenu not in final[currentMeal][currentLine]:
                final[currentMeal][currentLine][currentMenu] = []
            final[currentMeal][currentLine][currentMenu] += [j.strip() for j in pureText.split(',')]
            continue
        if pureText.startswith('VEG'):
            currentMenu = 'vegetarijanski'
            pureText = pureText.split(':')[1]
            if currentMeal not in final:
                final[currentMeal] = dict()
            if currentLine not in final[currentMeal]:
                final[currentMeal][currentLine] = dict()
            if currentMenu not in final[currentMeal][currentLine]:
                final[currentMeal][currentLine][currentMenu] = []
            final[currentMeal][currentLine][currentMenu] += [j.strip() for j in pureText.split(',')]
            continue
        if pureText.startswith('IZBOR'):
            currentMenu = 'izbor'
            pureText = pureText.split(':')[1]
            if currentMeal not in final:
                final[currentMeal] = dict()
            if currentLine not in final[currentMeal]:
                final[currentMeal][currentLine] = dict()
            if currentMenu not in final[currentMeal][currentLine]:
                final[currentMeal][currentLine][currentMenu] = []
            final[currentMeal][currentLine][currentMenu] += [j.strip() for j in pureText.split(',')]
            continue
        final[currentMeal][currentLine][currentMenu] += [j.strip() for j in pureText.split(',')]

output = ""

for i in final:
    for j in final[i]:
        for k in final[i][j]:
            for l in final[i][j][k]:
                output += "  {\n    \"timeType\": \"" + i + "\" ,\n    \"line\": \"" + j + "\" ,\n    \"offerType\": \"" + k + "\",\n    \"name\": \"" + l + "\"\n  },\n"
output = output[:len(output) - 2]
print("[ \n" + output + "\n]")
