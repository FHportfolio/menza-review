import re
import requests
from bs4 import BeautifulSoup

url = "http://www.cassandra.hr/studentski-menu/"
response = requests.get(url)
html = response.content

dateRegex = re.compile(".*,.(.|..)\.(.|..)\.....")
# dan, (1|01).(1|01).(2019)
soup = BeautifulSoup(html, "lxml")
p = soup.findAll('p')
# pureText = p.get_text()
menus = dict()

state = 0
currentMenu = ""

currentDate = ""

# states
# 0 is before date is read
# 1 is after date is read i expect menu or meal entry

for i in p:
    pureText = i.get_text()
    if state == 0:
        if dateRegex.match(pureText) is not None:
            currentDate = pureText[len(pureText) - 9:len(pureText)]
            state = 1
    elif state == 1:
        rows = pureText.split('\n')
        currentMenu = rows[0].strip()
        menus[currentMenu] = []
        for j in rows[1:]:
            menus[currentMenu] += [j]

output = "["
for i in menus:
    if (i[:4] == 'MENU'):
        for j in menus[i]:
            output += "{ \"timeType\": \"Ručak\", \"line\":\"line\", \"offerType\":\"" + i + "\", \"name\":\"" + j + "\"},\n"

url = "http://www.cassandra.hr/menu/"
response = requests.get(url)
html = response.content

soup = BeautifulSoup(html, "lxml")
p = soup.findAll('p')
# pureText = p.get_text()
menus = dict()

dateRegex = re.compile(".*,...\...\.....")
state = 0
currentMenu = ""
currentDate = ""

# states
# 0 is before date is read
# 1 is after date is read i expect menu or meal entry
specialLetters = "(š|Š|ž|Ž|ć|Ć|č|Č|đ|Đ)+"

acceptableCharRegex = re.compile("([a-zA-Z]+|(š|Š|ž|Ž|ć|Ć|č|Č|đ|Đ)+)(\s+|-+|/+|[a-zA-Z]+|,+|(š|Š|ž|Ž|ć|Ć|č|Č|đ|Đ)+)+")

for i in p:
    pureText = i.get_text().strip()
    # print(pureText)
    for j in pureText.split('\n'):
        if j != 'Cassandra' and j != 'Restoran':
            if acceptableCharRegex.match(j.strip()) is not None:
                output += "{ \"timeType\": \"Ručak\", \"line\":\"line\", \"offerType\": \"izbor\", \"name\":\"" + j + "\"},\n"

print(output[:len(output) - 2] + "]\n")
# r = requests.post("http://localhost:8080/dailyoffer/add/3", data=(output[:len(output) - 2] + "]\n").encode('utf-8'))
# r = requests.post("https://menza-review-backend.herokuapp.com/dailyoffer/add/3", data=(output[:len(output) - 2] + "]\n").encode('utf-8'))
# print(r.status_code)

# print(p)
