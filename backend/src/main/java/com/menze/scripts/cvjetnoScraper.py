import re
import requests
from bs4 import BeautifulSoup

url = "http://www.sczg.unizg.hr/prehrana/restorani/sd-cvjetno-naselje/"
response = requests.get(url)
html = response.content

soup = BeautifulSoup(html, "lxml")
nonBreakSpace = u'\xa0'
p = soup.findAll(['h2', 'h3', 'h5'])

dateRegex = re.compile(".*,...\...\.....")
state = 0
currentMenu = ""
currentDate = ""

specialLetters = "(š|Š|ž|Ž|ć|Ć|č|Č|đ|Đ)+"

acceptableCharRegex = re.compile("([a-zA-Z]+|(š|Š|ž|Ž|ć|Ć|č|Č|đ|Đ)+)(\s+|-+|/+|[a-zA-Z]+|,+|(š|Š|ž|Ž|ć|Ć|č|Č|đ|Đ)+)+")

ignoreFlag = True

currentMeal = "Doručak"
currentLine = ''
state = 0

final = dict()

for i in p:

    pureText = i.get_text().strip()
    pureText = pureText.replace(nonBreakSpace, '')
    for j in pureText.split('\n'):
        if j == 'VEČERA':
            currentMeal = j
            final[currentMeal] = dict()
        elif j == 'DORUČAK':
            currentMeal = j
            final[currentMeal] = dict()
        elif j == 'RUČAK':
            currentMeal = j
            final[currentMeal] = dict()
        if j.startswith("LINIJA I "):
            currentLine = 'Linija 1'
            currentMenu = 'menu'
            if currentLine not in final[currentMeal]:
                final[currentMeal][currentLine] = dict()
            j = [k.strip() for k in j.split(':')[1].strip().split(',')]
            final[currentMeal][currentLine][currentMenu] = j
            # print(currentMeal + " - " + currentLine, end=' :::')
            # print(j)
            continue
        if j.startswith('IZBOR'):
            currentMenu = 'izbor'
            if currentLine not in final[currentMeal]:
                final[currentMeal][currentLine] = dict()
            j = [k.strip() for k in j.split(':')[1].strip().split(',')]
            final[currentMeal][currentLine][currentMenu] = j
            # print("IZBOR - " + currentMeal + " - " + currentLine, end=' :::')

            # print(j)
            continue
        if j.startswith("LINIJA II "):
            currentLine = "Linija 2"
            currentMenu = 'menu'
            if currentLine not in final[currentMeal]:
                final[currentMeal][currentLine] = dict()
            j = [k.strip() for k in j.split(':')[1].strip().split(',')]
            final[currentMeal][currentLine][currentMenu] = j
            # print(currentMeal + " - " + currentLine, end=' :::')
            # print(j)
            continue
        if j.startswith("VEGETARIJANSKI MENU"):
            currentMenu = 'vegetarijanski'
            if currentLine not in final[currentMeal]:
                final[currentMeal][currentLine] = dict()
            j = [k.strip() for k in j.split(':')[1].strip().split(',')]
            final[currentMeal][currentLine][currentMenu] = j
        # print("vegani")
        # print(j)
output = ""

for i in final:
    for j in final[i]:
        for k in final[i][j]:
            for l in final[i][j][k]:
                output += "  {\n    \"timeType\": \"" + i + "\" ,\n    \"line\": \"" + j + "\" ,\n    \"offerType\": \"" + k + "\",\n    \"name\": \"" + l + "\"\n  },\n"
output = output[:len(output) - 2]
print("[ \n" + output + "\n]")
# requests.post("http://localhost:8080/dailyoffer/add/1", data=("[ \n" + output + "\n]").encode('utf-8'))
# requests.post("https://menza-review-backend.herokuapp.com/dailyoffer/add/1", data=("[ \n" + output + "\n]").encode('utf-8'))
