import requests
from bs4 import BeautifulSoup

url = "http://www.sczg.unizg.hr/prehrana/restorani/savska/"
response = requests.get(url)
html = response.content

soup = BeautifulSoup(html, "lxml")
nonBreakSpace = u'\xa0'
p = soup.findAll(['p'])

ignoreFlag = True

currentMeal = "Ručak"
currentMenu = ''

final = dict()

for i in p[:len(p) - 2]:
    pureText = i.get_text().replace(nonBreakSpace, "")
    if pureText == '':
        continue
    pureText = ' '.join(pureText.split())
    pureText = pureText.strip()
    if pureText.startswith("LINIJA LIJEVO RUČAK"):
        ignoreFlag = False
        final[currentMeal] = dict()
        continue
    if ignoreFlag == False:
        if pureText.startswith("MENU"):
            currentMenu = 'meni'
            final[currentMeal][currentMenu] = []
            continue
        if pureText.startswith("VEGETARIJANSKI"):
            currentMenu = 'vegetarijanski'
            final[currentMeal][currentMenu] = []
            continue
        if pureText.startswith('IZBOR') or pureText.startswith("PRILOZI"):
            currentMenu = 'izbor'
            final[currentMeal][currentMenu] = []
            continue
        if pureText.startswith("VEČERA"):
            currentMeal = 'VEČERA'
            final[currentMeal] = dict()
            continue
        if currentMenu != 'izbor':
            final[currentMeal][currentMenu] = [j.strip() for j in pureText.split(',')]
            continue
        else:
            final[currentMeal][currentMenu] += [pureText[2:].strip()]
            continue
# for i in final:
# 	print(i)
# 	for j in final[i]:
# 		print("    " + j)
# 		for k in final[i][j]:
# 			print("        " + k)
#

output = ""

for i in final:
    for j in final[i]:
        for k in final[i][j]:
            output += "  {\n    \"timeType\": \"" + i + "\" ,\n    \"line\": \"" + "linija" + "\" ,\n    \"offerType\": \"" + j + "\",\n    \"name\": \"" + k + "\"\n  },\n"
output = output[:len(output) - 2]
print("[ \n" + output + "\n]")
# requests.post("http://localhost:8080/dailyoffer/add/2", data=("[ \n" + output + "\n]").encode('utf-8'))
# requests.post("https://menza-review-backend.herokuapp.com/dailyoffer/add/2", data=("[ \n" + output + "\n]").encode('utf-8'))
