import re
import requests
from bs4 import BeautifulSoup

url = "http://www.cassandra.hr/menu/"
response = requests.get(url)
html = response.content

soup = BeautifulSoup(html, "lxml")
p = soup.findAll('p')
# pureText = p.get_text()
menus = dict()

dateRegex = re.compile(".*,...\...\.....")
state = 0
currentMenu = ""
currentDate = ""

# states
# 0 is before date is read
# 1 is after date is read i expect menu or meal entry
specialLetters = "(š|Š|ž|Ž|ć|Ć|č|Č|đ|Đ)+"

acceptableCharRegex = re.compile("([a-zA-Z]+|(š|Š|ž|Ž|ć|Ć|č|Č|đ|Đ)+)(\s+|-+|/+|[a-zA-Z]+|,+|(š|Š|ž|Ž|ć|Ć|č|Č|đ|Đ)+)+")

output = "["
for i in p:
    pureText = i.get_text().strip()
    # print(pureText)
    for j in pureText.split('\n'):
        if j != 'Cassandra' and j != 'Restoran':
            if acceptableCharRegex.match(j.strip()) is not None:
                output += "{ \"timeType\": \"Ručak\", \"line\":\"line\", \"offerType\": \"izbor\", \"name\":\"" + j + "\"},\n"
                print(j.strip())

print(output[:len(output) - 2] + "]\n")
# 	if state == 0:
# 		if dateRegex.match(pureText) is not None:
# 			currentDate = pureText[len(pureText)-9:len(pureText)]
# 			state = 1
# 	elif state == 1:
# 		rows = pureText.split('\n')
# 		currentMenu = rows[0].strip()
# 		menus[currentMenu] = []
# 		for j in rows[1:]:
# 			menus[currentMenu] += [j]

# print(currentDate)
# for i in menus:
# 	if(i[:4] == 'MENU'):
# 		print(i)
# 		print(menus[i])
# # print(p)
