package com.menze.services;

import com.menze.dto.RatingDTO;

public interface RatingService {
    double getAverageRatingByMenzaMealId(long menzaMealId);

    void saveRating(RatingDTO dto);

    int getMyRating(Long menzaMealId);
}
