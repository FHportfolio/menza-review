package com.menze.services;

import com.menze.entities.Menza;

import java.util.List;

public interface MenzaService {

    Menza getMenzaByID(Long ID);

    List<Menza> getAllMenza();

}
