package com.menze.services;

import com.menze.dto.DailyOfferDTO;
import com.menze.dto.MealDTO;

import java.io.IOException;
import java.util.List;

public interface DailyOfferService {

    List<DailyOfferDTO> gelAllByMenzaID(Long menzaId);

    void updateMenu(Long menzaId);

    List<MealDTO> runScript(String scriptLocation) throws IOException;

    void updateAllMenu();


}
