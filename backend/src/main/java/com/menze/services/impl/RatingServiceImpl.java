package com.menze.services.impl;

import com.menze.dto.RatingDTO;
import com.menze.dto.mappers.impl.RatingMapper;
import com.menze.entities.Rating;
import com.menze.repositories.RatingRepository;
import com.menze.services.RatingService;
import com.menze.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service

public class RatingServiceImpl implements RatingService {
    private final RatingRepository ratingRepository;
    private final RatingMapper ratingMapper;
    private final UserService userService;

    @Autowired
    public RatingServiceImpl(RatingRepository ratingRepository, RatingMapper ratingMapper, UserService userService) {
        this.ratingRepository = ratingRepository;
        this.ratingMapper = ratingMapper;
        this.userService = userService;
    }

    public double getAverageRatingByMenzaMealId(long menzaMealId) {
        return ratingRepository.getAverageRatingByMenzaMealId(menzaMealId).orElse(0.);
    }


    @Transactional
    public void saveRating(RatingDTO dto) {
        if (ratingRepository.existsByMenzaMealIDAndUserID(dto.getMenzaMealId(), userService.getCurrentUser().getID())) {
            ratingRepository.deleteByUserIDAndMenzaMealID(userService.getCurrentUser().getID(), dto.getMenzaMealId());
        }
        Rating rating = ratingMapper.dtoToEntity(dto);
        rating.setUser(userService.getCurrentUser());
        ratingRepository.save(rating);
    }

    public int getMyRating(Long menzaMealId){
        Rating rating = ratingRepository.findByUserIDAndMenzaMealID(userService.getCurrentUser().getID(), menzaMealId).orElse(null);
        return rating == null ? 0 : rating.getRating();
    }
}
