package com.menze.services.impl;

import com.menze.entities.MenzaMeal;
import com.menze.repositories.DailyOfferRepository;
import com.menze.repositories.MealRepository;
import com.menze.repositories.MenzaMealRepository;
import com.menze.services.MenzaMealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MenzaMealServiceImpl implements MenzaMealService {

    private final MenzaMealRepository menzaMealRepository;
    private final MealRepository mealRepository;
    private final DailyOfferRepository dailyOfferRepository;

    @Autowired
    public MenzaMealServiceImpl(MenzaMealRepository menzaMealRepository, MealRepository mealRepository, DailyOfferRepository dailyOfferRepository) {
        this.menzaMealRepository = menzaMealRepository;
        this.mealRepository = mealRepository;
        this.dailyOfferRepository = dailyOfferRepository;
    }

    @Override
    public MenzaMeal getMenzaMealById(Long id) {
        return menzaMealRepository.findById(id).orElse(null);
    }


}
