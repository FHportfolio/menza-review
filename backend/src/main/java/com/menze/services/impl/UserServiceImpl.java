package com.menze.services.impl;

import com.menze.dto.MessageDTO;
import com.menze.dto.UserDTO;
import com.menze.dto.mappers.impl.MessageMapper;
import com.menze.dto.mappers.impl.UserMapper;
import com.menze.entities.City;
import com.menze.entities.Message;
import com.menze.entities.User;
import com.menze.repositories.CityRepository;
import com.menze.repositories.MessageRepository;
import com.menze.repositories.PictureRepository;
import com.menze.repositories.UserRepository;
import com.menze.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Primary
public class UserServiceImpl implements UserService {

    private static final String USERNAME_NOT_FOUND = "User with email %s has not been registered.";

    private final UserRepository userRepository;
    private final MessageRepository messageRepository;
    private final MessageMapper messageMapper;
    private final CityRepository cityRepository;
    private final UserMapper userMapper;
    private final PictureRepository pictureRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, MessageRepository messageRepository, MessageMapper messageMapper, CityRepository cityRepository, UserMapper userMapper, PictureRepository pictureRepository) {
        this.userRepository = userRepository;
        this.messageRepository = messageRepository;
        this.messageMapper = messageMapper;
        this.cityRepository = cityRepository;
        this.userMapper = userMapper;
        this.pictureRepository = pictureRepository;
    }

    @Override
    public User getUserByEmail(@NotNull String email) {
        return userRepository.findByEmail(email).orElse(null);
    }

    @Override
    public User getUserById(@NotNull UUID id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email).orElse(null);

        if (user == null) {
            throw new UsernameNotFoundException(String.format(USERNAME_NOT_FOUND, email));
        }

        Set<GrantedAuthority> auth = new HashSet<>();
        auth.add(new SimpleGrantedAuthority(user.isAdmin() ? "ADMIN" : "USER"));
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPasswordHash(), auth);
    }

    public List<MessageDTO> getAllMessageDtoByEmail(String email) {
        return messageRepository.findAllByUserEmailOrderByTimestampDesc(email).stream().map(messageMapper::entityToDto).collect(Collectors.toList());
    }

    public User getCurrentUser() {
        return userRepository.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName()).orElse(null);
    }

    public UserDTO getCurrentUserDTO() {
        return userMapper.entityToDto(getCurrentUser());
    }


    public boolean changeDefaultCity(Long cityId) {
        User currentUser = getCurrentUser();
        City newCity = cityRepository.findById(cityId).orElse(null);
        if (newCity == null) {
            return false;
        }
        currentUser.setCity(newCity);
        userRepository.save(currentUser);
        return true;
    }

    @Override
    public boolean userExistsWithUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    @Override
    public void sendMessage(MessageDTO messageDTO, Long imageId) {
        User user = pictureRepository.findById(imageId).orElse(null).getUser();
        messageDTO.setUserId(user.getID());
        Message message = messageMapper.dtoToEntity(messageDTO);
        message.setTimestamp(new Timestamp(System.currentTimeMillis()));
        message.setRead(false);
        messageRepository.save(message);
    }

    public boolean userExistsWithEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    public void saveUser(UserDTO dto) {
        userRepository.save(userMapper.dtoToEntity(dto));
    }

    public List<MessageDTO> getAllMessageDtoByEmailPageable(Pageable pageable) {
        return messageRepository.findAllByUserEmailOrderByTimestampDesc(getCurrentUser().getEmail(), pageable).stream().map(messageMapper::entityToDto).collect(Collectors.toList());
    }

    public boolean readMessage(Long messageId) {
        Message message = messageRepository.findById(messageId).orElse(null);
        if (message != null && message.getUser().getID().equals(getCurrentUser().getID())) {
            message.setRead(true);
            messageRepository.save(message);
            return true;
        }
        return false;
    }
}
