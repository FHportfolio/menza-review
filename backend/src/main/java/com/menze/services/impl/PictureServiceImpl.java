package com.menze.services.impl;

import com.menze.dto.PictureDTO;
import com.menze.dto.mappers.impl.PictureMapper;
import com.menze.entities.Picture;
import com.menze.repositories.PictureRepository;
import com.menze.services.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class PictureServiceImpl implements PictureService {

    private final PictureRepository pictureRepository;
    private final PictureMapper pictureMapper;

    @Autowired
    public PictureServiceImpl(PictureRepository pictureRepository, PictureMapper pictureMapper) {
        this.pictureRepository = pictureRepository;
        this.pictureMapper = pictureMapper;
    }

    @Override
    public void approve(long pictureId) {
        Picture picture = pictureRepository.findById(pictureId).orElse(null);
        if (picture == null) {
            throw new EntityNotFoundException();
        }
        picture.setApproved(true);
        pictureRepository.save(picture);
    }

    @Override
    public byte[] getImageById(Long id) {
        return pictureRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("image, doesn't exist")).getImage();
    }

    @Override
    public void savePicture(PictureDTO pictureDTO) {
        Picture entity = pictureMapper.dtoToEntity(pictureDTO);
        entity.setTimestamp(new Timestamp(System.currentTimeMillis()));
        entity.setApproved(false);
        pictureRepository.save(entity);
    }

    @Override
    @Transactional
    public List<PictureDTO> getAllPictureByMenzaMealId(Long menzaMealId) {
        return pictureRepository.findAllByMenzaMealIDAndApproved(menzaMealId, true).stream().map(pictureMapper::entityToDto).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<PictureDTO> findAllPictureByApproved(boolean isApproved) {
        return pictureRepository.findAllByApproved(isApproved).stream().map(pictureMapper::entityToDto).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void deletePicture(Long pictureId) {
        pictureRepository.findById(pictureId).orElseThrow(() -> new EntityNotFoundException("picture doesn't exist"));
        pictureRepository.removeById(pictureId);
    }
}
