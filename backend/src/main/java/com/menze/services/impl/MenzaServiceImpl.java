package com.menze.services.impl;

import com.menze.dto.MenzaDTO;
import com.menze.dto.mappers.impl.MenzaMapper;
import com.menze.entities.Menza;
import com.menze.repositories.MenzaRepository;
import com.menze.services.MenzaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
@Primary
public class MenzaServiceImpl implements MenzaService {

    private final MenzaRepository menzaRepository;
    private final MenzaMapper menzaMapper;

    @Autowired
    public MenzaServiceImpl(MenzaRepository menzaRepository, MenzaMapper menzaMapper) {
        this.menzaRepository = menzaRepository;
        this.menzaMapper = menzaMapper;
    }

    @Override
    public Menza getMenzaByID(Long ID) {
        return menzaRepository.findById(ID).orElse(null);
    }

    @Override
    public List<Menza> getAllMenza() {
        return menzaRepository.findAll();
    }

    public List<MenzaDTO> getAllMenzaDtoByCityId(Long cityId) {
        return menzaRepository.findAllByCityID(cityId).stream().map(menzaMapper::entityToDto).collect(Collectors.toList());
    }
}
