package com.menze.services.impl;

import com.menze.Konstante;
import com.menze.dto.DailyOfferDTO;
import com.menze.dto.MealDTO;
import com.menze.dto.mappers.impl.DailyOfferMapper;
import com.menze.entities.DailyOffer;
import com.menze.entities.Meal;
import com.menze.entities.Menza;
import com.menze.entities.MenzaMeal;
import com.menze.repositories.DailyOfferRepository;
import com.menze.repositories.MealRepository;
import com.menze.repositories.MenzaMealRepository;
import com.menze.repositories.MenzaRepository;
import com.menze.services.DailyOfferService;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DailyOfferServiceImpl implements DailyOfferService {

    private final DailyOfferRepository dailyOfferRepository;
    private final DailyOfferMapper dailyOfferMapper;
    private final MenzaMealRepository menzaMealRepository;
    private final MealRepository mealRepository;
    private final MenzaRepository menzaRepository;

    @Autowired
    public DailyOfferServiceImpl(DailyOfferRepository dailyOfferRepository, DailyOfferMapper dailyOfferMapper, MenzaMealRepository menzaMealRepository, MealRepository mealRepository, MenzaRepository menzaRepository) {
        this.dailyOfferMapper = dailyOfferMapper;
        this.dailyOfferRepository = dailyOfferRepository;
        this.menzaMealRepository = menzaMealRepository;
        this.mealRepository = mealRepository;
        this.menzaRepository = menzaRepository;
    }

    public List<DailyOfferDTO> gelAllByMenzaID(Long menzaId) {
        return dailyOfferRepository.findAllByMenzaID(menzaId).stream().map(dailyOfferMapper::entityToDto).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void updateMenu(Long menzaId) {
        Menza menza = menzaRepository.findById(menzaId).orElse(null);
        List<MealDTO> mealDTOList = null;
        try {
            mealDTOList = runScript(menza.getScriptLocation());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(mealDTOList == null) {
            return;
        }
        dailyOfferRepository.removeAllByMenzaID(menzaId);

        for (MealDTO i : mealDTOList) {
            DailyOffer dailyOffer = new DailyOffer();

            dailyOffer.setMenza(menza);
            dailyOffer.setLine(i.getLine());
            dailyOffer.setOfferType(i.getOfferType());
            dailyOffer.setTimeType(i.getTimeType());
            dailyOffer.setMenzaMeal(menzaMealRepository.findFirstByMealNameAndMenzaID(i.getName(), menza.getID()).orElse(null));

            if (dailyOffer.getMenzaMeal() == null) {
                Meal meal = mealRepository.findByName(i.getName()).orElse(null);
                if (meal == null) {
                    meal = new Meal();
                    meal.setName(i.getName());
                    mealRepository.save(meal);
                }
                MenzaMeal menzaMeal = new MenzaMeal();
                menzaMeal.setMeal(meal);
                menzaMeal.setMenza(menza);
                menzaMealRepository.save(menzaMeal);

                dailyOffer.setMenzaMeal(menzaMeal);
            }
            dailyOfferRepository.save(dailyOffer);
        }
    }

    @Scheduled(fixedDelay = 1000 * 60 * 20)
    @Transactional
    public void updateAllMenu() {
        System.out.println("Updating menu for all menza");
        menzaRepository.findAll().forEach(menza -> {
            updateMenu(menza.getID());
        });
    }

    @Override
    public List<MealDTO> runScript(String scriptLocation) throws IOException {
        StringBuilder scriptOutput = new StringBuilder();
        StringBuilder scriptErrorOutput = new StringBuilder();
        try {
            Process process = Runtime.getRuntime().exec("python3 " + Konstante.PROJECT_ROOT + scriptLocation); //Konstante.SAVA_SCRAPER zamijeniti sa scriptLocation
            BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader errorBR = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String line = "";
            while((line = br.readLine()) != null) {
                scriptOutput.append(line);
            }
            System.err.println(process.isAlive());
            while((line = errorBR.readLine())!=null){
                scriptErrorOutput.append(line);
            }
        } catch (Exception e) {
            System.out.println(e);
            System.out.println("Couldn't parse menza menu");
        }
        ObjectMapper objectMapper = new ObjectMapper();
        List<MealDTO> mealDTOList;
        try {
            mealDTOList = objectMapper.readValue(scriptOutput.toString(), objectMapper.getTypeFactory().constructCollectionType(List.class, MealDTO.class));
        }
        catch (Exception e) {
            System.err.println(e);
            System.out.println(scriptOutput);
            System.out.println(":::::::::::::::");
            System.out.println(scriptErrorOutput);
            return null;
        }
        return mealDTOList;
    }
}
