package com.menze.services.impl;

import com.menze.dto.CommentDTO;
import com.menze.dto.mappers.impl.CommentMapper;
import com.menze.entities.Comment;
import com.menze.repositories.CommentRepository;
import com.menze.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;
    private final UserServiceImpl userService;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository, CommentMapper commentMapper, UserServiceImpl userService) {
        this.commentRepository = commentRepository;
        this.commentMapper = commentMapper;
        this.userService = userService;
    }

    @Override
    public List<CommentDTO> getCommentByMenzaMealIdPageable(Long menzaMealId, Pageable pageable) {
        return commentRepository.findAllByMenzaMealIDOrderByTimestampDesc(menzaMealId, pageable).stream().map(commentMapper::entityToDto).collect(Collectors.toList());
    }

    public List<CommentDTO> getAllCommentByMenzaMealId(Long menzaMealId) {
        return commentRepository.findAllByMenzaMealID(menzaMealId).stream().map(commentMapper::entityToDto).collect(Collectors.toList());
    }

    public void deleteCommentById(Long commentId) {
        commentRepository.findById(commentId).orElseThrow(() -> new EntityNotFoundException("comment doesn't exist"));
        commentRepository.deleteById(commentId);
    }

    public void saveComment(CommentDTO dto) {
        if (dto.getText().trim().length() == 0) {
            throw new IllegalArgumentException("Length can't be 0");
        }
        Comment comment = commentMapper.dtoToEntity(dto);
        comment.setUser(userService.getCurrentUser());
        comment.setTimestamp(new Timestamp(System.currentTimeMillis()));
        commentRepository.save(comment);
    }
}
