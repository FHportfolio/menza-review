package com.menze.services;

import com.menze.dto.MessageDTO;
import com.menze.entities.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface UserService extends UserDetailsService {

    /**
     * @param email users email
     * @return user with provided email
     */
    User getUserByEmail(@NotNull String email);

    /**
     * @param id users id
     * @return user with provided id
     */
    User getUserById(@NotNull UUID id);

    /**
     * @return current user
     */
    User getCurrentUser();

    boolean userExistsWithUsername(String username);

    void sendMessage(MessageDTO messageDTO, Long pictureId);
}
