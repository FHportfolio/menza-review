package com.menze.services;

import com.menze.dto.PictureDTO;

import java.util.List;

public interface PictureService {
    void approve(long pictureId);

    byte[] getImageById(Long id);

    void savePicture(PictureDTO pictureDTO);

    List<PictureDTO> getAllPictureByMenzaMealId(Long menzaMealId);

    List<PictureDTO> findAllPictureByApproved(boolean isApproved);

    void deletePicture(Long pictureId);
}