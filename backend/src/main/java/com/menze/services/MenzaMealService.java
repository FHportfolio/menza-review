package com.menze.services;

import com.menze.entities.MenzaMeal;

public interface MenzaMealService {

    MenzaMeal getMenzaMealById(Long id);
}
