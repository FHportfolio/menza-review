package com.menze.services;

import com.menze.dto.CommentDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CommentService {

    List<CommentDTO> getAllCommentByMenzaMealId(Long menzaMealId);

    void deleteCommentById(Long commentId);

    void saveComment(CommentDTO dto);

    List<CommentDTO> getCommentByMenzaMealIdPageable(Long menzaMealId, Pageable pageable);
}
