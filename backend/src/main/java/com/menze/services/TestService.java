package com.menze.services;

import com.menze.entities.City;
import com.menze.repositories.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * service to test ideas
 */

@Service
public class TestService {

    private final CityRepository cityRepository;

    @Autowired
    public TestService(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    public City getCityById(Long id) {
        return cityRepository.findById(id).orElse(null);
    }
}
