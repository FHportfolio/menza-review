package com.menze.services;

import com.menze.entities.City;

import java.util.List;

public interface CityService {

    List<City> getAllCities();

}
