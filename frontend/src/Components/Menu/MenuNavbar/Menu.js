import React, {Component} from 'react';
import {Button, TabPane} from "reactstrap";
import MealCommentUpload from "./MealCommentUpload";
import RatingComponent from "./RatingComponent";
import connect from "react-redux/es/connect/connect";

// var MediaQuery = require('react-responsive');

export class MenuDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            restaurantMeals: [],
            comments: [],
            index: -1,
            selected: '',
            ratings: []
        }
    }

    componentDidMount() {
        this.setState({selected: this.props.offers[0]});
    }

    handleMealSelected = (index, restMeal) => {
        if (restMeal === this.state.selected) {
            this.setState({index:-1, selected:""})
        }
        else {
            this.setState({
                index: index,
                selected: restMeal,
            });
        }
    };

    render() {
        let mapper = this.props.offers
            .map((restMeal, index) => {
                return (
                    <div key={index} style={{display:'flex', flexDirection: 'column', flex:1, margin:20, backgroundColor:'white'}}>
                        <Button key={index} active={this.state.selected === restMeal} outline color="primary" onClick={() => this.handleMealSelected(index, restMeal)}>{restMeal.name}</Button>
                            { this.state.selected === restMeal ?
                                <MealCommentUpload currentMeal={this.state.selected}
                                                   comments={this.state.comments}
                                                   updateComments={this.updateComments}
                                >
                                    {this.state.selected !== ''  && <RatingComponent currentMeal={this.state.selected}
                                                                                     ratings={this.state.ratings}
                                                                                     user ={this.props.user}
                                    />
                                    }
                                </MealCommentUpload> :
                                null
                            }
                    </div>
                )
            });
        return (
            <TabPane tabId={this.props.index} className="">
                {/*<div style={{display:'flex', flexDirection: 'column', flex:1, margin:20, backgroundColor:'white'}}>*/}
                    {mapper}
                {/*</div>*/}
            </TabPane>
        )

    }
}

export default connect((state, ownProps) => ({
    user: state.user,
    comments: state.allComments
}))(MenuDetails)