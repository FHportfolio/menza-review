import React, {Component} from 'react';
import classnames from 'classnames';
import {Nav, NavItem, NavLink, TabContent} from "reactstrap";
import './MenuNavbar.css'
import NavigationBar from "../../NavigationBar/NavigationBar";
import axios from "axios";
import {dailyOfferByMenzaId, menzaByIdURL} from "../../../Util/paths";
import Menu from "./Menu";
import {isLoggedIn} from "../../../Security/Security";
import connect from "react-redux/es/connect/connect";
import {OFFERS, RESTAURANT, updateUser} from "../../../Util/ReduxStore";
import {store} from "../../App/App";


export class MenuNavbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: '0',
        }
    }

    componentDidMount() {
        axios.get(dailyOfferByMenzaId(this.props.match.params.id))
            .then(res => {
                store.dispatch({type: OFFERS, payload: res.data});
            });
        axios.get(menzaByIdURL(this.props.match.params.id))
            .then(res => {
                store.dispatch({type: RESTAURANT, payload: res.data});
            });

        if (isLoggedIn()) {
            updateUser();
        }
    }

    toggle = (tab) => {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    };

    calculateDistinct = () => {
        return new Set(this.props.offers.map(item => item.offerType));
    };

    render() {
        let set = Array.from(this.calculateDistinct()).sort();

        let entries = set.map((entry, index) =>
            <NavItem key={index}>
                <NavLink href="#" className={classnames({active: this.state.activeTab === index.toString()})}
                         onClick={() => {
                             this.toggle(index.toString())
                         }}
                >
                    {entry}
                </NavLink>
            </NavItem>
        );

        let menuDetails = set.map((offerType, index) =>
            <Menu type={offerType}
                  offers={this.props.offers.filter(offer => offer.offerType === offerType)}
                  index={index.toString()}
                  restaurantMeals={this.props.offers.filter(item => item.offerType === offerType)}
                  key={offerType}
            />
        );

        return (
            <div className="menuNavbar">
                <NavigationBar userContent={true}
                               id={this.props.restaurants.id}
                               name={this.props.restaurants.name}
                               isMenu={false}
                               history={this.props.history}
                />
                <Nav tabs>
                    {entries}
                </Nav>
                <TabContent activeTab={this.state.activeTab}>
                    {menuDetails}
                </TabContent>
            </div>
        )
    }

}

export default connect(state => ({
    offers: state.offers,
    restaurants: state.restaurants,
    user: state.user
}))(MenuNavbar)