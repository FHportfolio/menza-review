import React, {Component} from 'react';
import Rating from "react-rating";
import batakPrazni from '../../../Images/batakPrazni1.png';
import batakPuni from '../../../Images/batakPuni1.png';
import {Alert, Label} from "reactstrap";
import {getAuthorizationHeader, isLoggedIn} from "../../../Security/Security";
import {avgratingURL, ratingMyRatingByMenzaMealId, ratingURL} from "../../../Util/paths";
import axios from "axios";


export class RatingComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            rating: 0,
            avg: 0,
            visible: false
        }
    }

    componentDidMount() {
        this.getRating(this.props.currentMeal.menzaMealId);
        this.getMyRating(this.props.currentMeal.menzaMealId)
    }

    getMyRating(menzaMealId) {
        axios({
            url:ratingMyRatingByMenzaMealId(menzaMealId),
            method:'get',
            headers: getAuthorizationHeader()
        }).then(r => this.setState({rating:r.data}))
            .catch(e => {throw e});
    }

    getRating(menzaMealId) {
        axios({
            url:avgratingURL(menzaMealId),
            method: 'get'
        }).then(r => this.setState({avg: r.data}))
            .catch(e => {throw e});
    }

    handleRatingChange = (value) => {
        this.setState({
            rating: value
        })
    };

    componentWillReceiveProps(nextProps, nextContext) {
        this.getRating(nextProps.currentMeal.menzaMealId);
        this.getMyRating(nextProps.currentMeal.menzaMealId);
    }


    alertTimeout = () => {
        this.setState({
            visible: false
        })
    };
    handleSaveRating = (value) => {
        let ratingDTO = {
            userId: this.props.user.id,
            menzaMealId: this.props.currentMeal.menzaMealId,
            rating: value
        };
        axios({
            url:ratingURL,
            method:'post',
            data:ratingDTO,
            headers:getAuthorizationHeader()
        }).then(r => {
            this.getRating(this.props.currentMeal.menzaMealId);
            this.getMyRating(this.props.currentMeal.menzaMealId);
            this.setState({rating:value, visible:true}, () => setTimeout(() => this.setState({visible:false}), 2000));
        }).catch(e => {throw e})
    };

    render() {
        return (
            <div style={{display:"flex", flexDirection:"column"}}>
                {isLoggedIn() &&
                    <div style={{display:"flex", flexDirection:"column"}}>
                        <Label>Ocijeni jelo</Label>
                        <div style={{display:'flex', flexWrap:'nowrap'}}>
                            <Rating
                                initialRating={this.state.rating}
                                emptySymbol={<img style={{height: "60px"}} src={batakPrazni} alt="-" className="icon"/>}
                                fullSymbol={<img style={{height: "60px"}} src={batakPuni} alt="#" className="icon"/>}
                                fractions={1}
                                onChange={this.handleRatingChange}
                                onClick={(value) => this.handleSaveRating(value)}
                            />
                        </div>
                    </div>
                }
                <div style={{display:"flex", flexDirection:"column"}}>
                    <Label>Prosječna ocjena</Label>
                    <div style={{display:'flex', flexDirection:'column'}}>
                        <Rating
                            initialRating={this.state.avg}
                            emptySymbol={<img style={{height: "60px"}} src={batakPrazni} alt="-" className="icon"/>}
                            fullSymbol={<img style={{height: "60px"}} src={batakPuni} alt="#" className="icon"/>}
                            fractions={2}
                            readonly
                        />
                    </div>
                </div>
                <Alert color="success" isOpen={this.state.visible}>
                    Ocjena spremljena
                </Alert>
            </div>
        )
    }

}


export default RatingComponent;