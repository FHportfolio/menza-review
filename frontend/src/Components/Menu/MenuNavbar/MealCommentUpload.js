import React, {Component} from 'react';
import {Alert, Button, Card, CardFooter, CardHeader, CardImg, Col, Form, FormGroup, Input, Label} from "reactstrap";
import './MealCommentUpload.css';
import {commentByMenzaMealId, commentsURL, imageByImageId, imagesByMenzaMealId, pictureURL} from "../../../Util/paths";
import {_request, getAuthorizationHeader, isLoggedIn} from "../../../Security/Security";
import connect from "react-redux/es/connect/connect";
import axios from "axios";

export class MealCommentUpload extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comments: [],
            comment: "",
            visibleComment: false,
            visibleImage: false,
            image: null,
            error: "",
            images: [],
            uploadCommentImageFlag:true
        }
    }

    getComments(menzaMealId) {
        axios({url: commentByMenzaMealId(menzaMealId), method: 'get'})
            .then(r => {
                let comments = r.data.sort((item1, item2) => item2.timestamp < item1.timestamp ? -1 : item2.timestamp > item1.timestamp ? 1 : 0).map(comment => {
                    let date = new Date(comment.timestamp);
                    return (
                        <div key={comment.id} style={{marginTop:20}}>
                            <Col>
                                <Card>
                                    <CardHeader>Korisnik {comment.username} kaže</CardHeader>
                                    <div style={{padding:10}}>{comment.text}</div>
                                    <CardFooter>{date.toUTCString()}</CardFooter>
                                </Card>
                            </Col>
                        </div>
                    )
                });
                this.setState({comments: comments});
                return true;
            }).catch(e => {
            throw e
        });
    };

    getImages(menzaMealId) {
        axios({url: imagesByMenzaMealId(menzaMealId), method: 'get'})
            .then(r => {
                let images = r.data.sort((item1, item2) => item2.timestamp < item1.timestamp ? -1 : item2.timestamp > item1.timestamp ? 1 : 0).map(image => {
                    let date = new Date(image.timestamp);
                    return (
                        <div key={image.id} style={{marginTop:20}}>
                            <Card>
                                <CardHeader>User {image.username} says</CardHeader>
                                <CardImg src={imageByImageId(image.id)} />
                                <div style={{padding:10}}>{image.description}</div>
                                <CardFooter>{date.toUTCString()}</CardFooter>
                            </Card>
                        </div>
                    )
                });
                this.setState({images: images});
                return true;
            }).catch(e => {
            throw e
        });
    }

    componentDidMount() {
        this.getComments(this.props.currentMeal.menzaMealId);
        this.getImages(this.props.currentMeal.menzaMealId);
    }

    handleChange = (event) => {
        this.setState({
            comment: event.target.value,
            error: ""
        })
    };

    handleImageChange = async (event) => {
        await this.setState({
            image: event.target.files[0],
            error: ""
        });
    };

    alertTimeoutImage = () => {
        this.setState({
            visibleImage: false
        })
    };

    alertTimeoutComment = () => {
        this.setState({
            visibleComment: false
        })
    };

    handleSave = () => {
        if (this.state.comment.trim() === "") {
            this.setState({error: "Comment can not be empty"});
            return ;
        }
        let commentDTO = {
            userId: this.props.user.id,
            text: this.state.comment,
            menzaMealId: this.props.currentMeal.menzaMealId,
        };
        _request(commentsURL, 'POST', commentDTO, getAuthorizationHeader())
            .then(() => {
                this.setState({
                    visibleComment: true
                }, () => setTimeout(() => this.alertTimeoutComment(), 2500));
                this.getComments(this.props.currentMeal.menzaMealId);
            });
    };

    handleImageSave = () => {
        if(this.state.image === null) {
            this.setState({error: "Image not selected"});
            return ;
        }
        let data = {
            menzaMealId: this.props.currentMeal.menzaMealId,
            image: this.state.image,
            userId: this.props.user.id,
            description: this.state.comment
        };

        let fr = new FileReader();

        fr.onload = (event) => {
            data.image = [];
            (new Int8Array(event.target.result)).forEach((value, index) => {
                data.image.push(value);
            });
            axios({
                url: pictureURL,
                method: 'post',
                data: data,
                headers: getAuthorizationHeader()
            }).then(r => {
                this.setState({
                    visibleImage: true,
                    comment:""
                }, () => setTimeout(() => this.alertTimeoutImage(), 2500));
                this.getImages(this.props.currentMeal.menzaMealId);
            }).catch(e => {throw e});
        };
        fr.readAsArrayBuffer(this.state.image);
    };

    componentWillReceiveProps(nextProps, nextContext) {
        this.getComments(nextProps.currentMeal.menzaMealId);
    }

    render() {
        return (
            <div style={{display:"flex", flexDirection:"column", marginTop:10}} >
                {this.props.children}
                <div>
                    <div>
                        <div style={{display:'flex', flexDirection:'row', justifyContent:'space-between', alignContent:'stretch'}}>
                            <div style={{flex:1, display:'flex', flexDirection:'column', backgroundColor:'white'}}>
                               <Button active={this.state.uploadCommentImageFlag} outline color="primary" onClick={() => this.setState({uploadCommentImageFlag:true})}>Komentari</Button>
                            </div>
                            <div style={{flex:1, display:'flex', flexDirection:'column', backgroundColor:'white'}}>
                                <Button active={!this.state.uploadCommentImageFlag} outline color="primary" onClick={() => this.setState({uploadCommentImageFlag:false})}>Galarija</Button>
                            </div>
                        </div>
                        {
                            this.state.uploadCommentImageFlag ? (
                                    <div style={{display:"flex", flexDirection:"column", margin:5}}>
                                        {isLoggedIn() &&
                                        <div>
                                            <div style={{display:"flex", flexDirection:"column"}}>
                                                <Form>
                                                    <FormGroup>
                                                        <Input type="textarea" placeholder="Komentiraj..."
                                                               onChange={this.handleChange}/>
                                                    </FormGroup>
                                                </Form>
                                            </div>
                                            <div style={{display:"flex", flexDirection:"row", justifyContent:"space-between", margin:10}}>
                                                <Button outline color="primary" onClick={this.handleSave}>Komentiraj</Button>
                                            </div>
                                            </div>}

                                            <div style={{marginTop:5, marginBottom:5}}>
                                                <Label for="comments">Komentari ({this.state.comments.length})</Label>
                                            </div>
                                            <div style={{marginTop:5, marginBottom:5}}>
                                                {this.state.comments}
                                            </div>
                                    </div>) :
                                (
                                    <div style={{display:"flex", flexDirection:"column", margin:5}}>
                                        {isLoggedIn() &&
                                        <div>
                                            <div style={{display: "flex", flexDirection: "column"}}>
                                                <Input type="textarea" placeholder="Komentiraj..."
                                                       onChange={this.handleChange}/>
                                            </div>
                                            <div style={{
                                                display: "flex",
                                                flexDirection: "row",
                                                justifyContent: "space-between",
                                                margin: 10
                                            }}>
                                                <input type='file' id='multi' onChange={this.handleImageChange}
                                                       multiple/>
                                                <Button outline color="primary" onClick={this.handleImageSave}>Učitaj
                                                    sliku</Button>
                                            </div>
                                        </div>
                                        }
                                        <div style={{margin:5}}>
                                            <Label for="images">Galerija ({this.state.images.length})</Label>
                                        </div>
                                        <div style={{margin:5}}>
                                            {this.state.images}
                                        </div>
                                    </div>)}
                    </div>
                </div>
                <div style={{display:"flex", flexDirection:"column"}}>
                    <div style={{display:"flex", flexDirection:"row"}}>
                        <Alert style={{margin:10}} color="danger" isOpen={this.state.error !== ""}>
                            {this.state.error}
                        </Alert>
                        <Alert color="success" isOpen={this.state.visibleComment}>
                            Komentar objavljen
                        </Alert>
                        <Alert color="success" isOpen={this.state.visibleImage}>
                            Slika poslana na odobravanje
                        </Alert>
                    </div>
                </div>
            </div>
        );
    }

}

export default connect((state, ownProps) => ({
    user: state.user,
}))(MealCommentUpload)