import React, {Component} from 'react';
import {Button, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Alert} from "reactstrap";
import './RegisterForm.css';
import {getAuthorizationHeader} from "../../../Security/Security";
import {registerURL} from "../../../Util/paths";
import validator from 'validator';
import axios from 'axios';

export class RegisterForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: this.props.register,
            username: "",
            email: "",
            password: "",
            confirmPassword:"",
            city: "",
            redirect:"",
            errors: [],
            visibleRegistrationErrors: false
        }
    }

    toggle = () => {
        this.setState({
            modal: !this.state.modal
        })
    };

    handleEmailChange = (event) => {
        let value = event.target.value;
        this.setState({
            email: value
        })

    };

    handleUsernameChange = (event) => {
        let value = event.target.value;
        this.setState({
            username: value
        })

    };

    handlePasswordChange = (event) => {
        let value = event.target.value;
        this.setState({
            password: value
        })

    };

    handleConfirmPasswordChange = (event) => {
        let value = event.target.value;
        this.setState({
            confirmPassword:value
        })
    };

    handleCityChanged = (event) => {
        let value = event.target.value;
        this.setState({
            city: value
        });
    };

    handleRegistration = () => {
        let errors = [];
        if(!validator.isEmail(this.state.email)){
            errors.push("Uneseni email nije ispravan");
        }
        if(this.state.confirmPassword !== this.state.password){
            errors.push("Lozinke se ne podudaraju");
        }
        if(this.state.password.length < 8) {
            errors.push("Lozinka mora imati barem 8 znakova")
        }
        if(errors.length !== 0){
            this.setState({errors:errors});
            return ;
        }
        let user = {
            username : this.state.username,
            password : this.state.password,
            cityId: this.state.city ? this.state.city : this.props.cities[0].id,
            email: this.state.email
        };
        axios({
            url:registerURL,
            method:'POST',
            data:user,
            headers:getAuthorizationHeader()
        }).then(r => {
            if(r.status === 409) {
                this.setState({errors: r.data === "" ? [] : r.data});
                return;
            }
            this.toggle();
        }).catch(e => {
            this.setState({errors: e.response.data}, () => setTimeout(() => this.setState({errors:[]}), 2500));
        });
    };


    render() {
        if (this.props.hidden) {
            return ""
        } else {
            return (
                <div className="RegisterForm">
                    <span onClick={this.toggle}>Nemaš račun? Registriraj se</span>
                    <Modal isOpen={this.state.modal} toggle={this.toggle}>
                        <ModalHeader toggle={this.toggle}>Register</ModalHeader>
                        <ModalBody>
                            <Form className="Register-Form">
                                <FormGroup>
                                    <Label for="username">Korisničko ime</Label>
                                    <Input type="uname" name="uname" onChange={this.handleUsernameChange} id="uname"
                                           placeholder="Korisničko ime..."/>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="password">Lozinka</Label>
                                    <Input type="password" name="password" onChange={this.handlePasswordChange}
                                           id="password" placeholder="Lozinka..."/>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="password">Ponovi lozinku</Label>
                                    <Input type="password" name="password" onChange={this.handleConfirmPasswordChange}
                                           id="confirmPassword" placeholder="Ponovi lozinku..."/>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="email">Email</Label>
                                    <Input type="email" name="email" onChange={this.handleEmailChange} id="email"
                                           placeholder="Unesi email"/>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="cities">Odaberi zadani grad</Label>
                                    <Input type="select" name="cities" onChange={this.handleCityChanged} id="cities">
                                        {this.props.cities !== undefined ? this.props.cities.map(city =>
                                            <option key={city.id} value={city.id}>{city.name}</option>) : null
                                        }
                                    </Input>
                                </FormGroup>
                            </Form>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" onClick={this.handleRegistration}>Register</Button>{' '}
                            <Button color="primary" onClick={this.toggle}>Cancel</Button>
                        </ModalFooter>
                        <div style={{paddingLeft:15, paddingRight:15}}>
                        {this.state.errors.map((error, index) =>
                            <Alert key={index} color="danger" isOpen={true}>
                                {error}
                            </Alert>
                        )}
                        </div>
                    </Modal>
                </div>
            )
        }
    }
}

export default RegisterForm;