import React, {Component} from 'react';
import {Col, Container, Row} from "reactstrap";
import Logo from '../../Images/logo-logo.svg';
import './StartPage.css';
import CityPicker from './CityPicker/CityPicker';
import LoginForm from "./LoginForm/LoginForm";
import {connect} from 'react-redux'
import {updateUser} from "../../Util/ReduxStore";
import {isLoggedIn} from "../../Security/Security";
import {Redirect} from "react-router-dom";
import axios from "axios";
import {allCitiesURL} from "../../Util/paths";
import zagreb from "../../Images/zagreb.jpg";
import osijek from "../../Images/osijek.jpg";
import split from "../../Images/split.jpg";
import Loader from 'react-loader-spinner'

export class StartPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: "",
            cities: [],
            loadedUser:false,
            loadedCities:false
        }
    }

    componentDidMount() {
        if (isLoggedIn()) {
            updateUser().then(r => {
                this.setState({redirect: "redirect", loadedUser:true});
            })
                .catch(e => {this.setState({loadedUser:true})});
        }
        else {
            this.setState({loadedUser:true});
        }

        axios({
            url:allCitiesURL,
            method:'get'
        }).then(r => {
            this.setState({cities:r.data});
            let cityData = [];
            this.state.cities.forEach(city => {
                if (city.name === "Zagreb") {
                    cityData.push({
                        id: city.id,
                        name: city.name,
                        image: zagreb
                    })
                } else if (city.name === "Osijek") {
                    cityData.push(
                        {
                            id: city.id,
                            name: city.name,
                            image: osijek
                        })
                } else {
                    cityData.push(
                        {
                            id: city.id,
                            name: city.name,
                            image: split
                        })
                }
            });
            this.setState({cities:cityData, loadedCities:true});
        })
            .catch(e => {throw e});

    }

    render() {
        if (!this.state.loadedUser && !this.state.loadedCities) {
            return <Loader
                type="Puff"
                color="#00BFFF"
                height="100"
                width="100"
            />
        }
        if (this.state.redirect !== "") {
            return <Redirect to={`/home/${this.props.user.cityId}`} />
        }
        return (
            <div className="StartPage">
                <Container>
                    <Row>
                        <Col md={{size:4}}/>
                        <Col className="text-center" sm={{size:12}} md={{size:4}} >
                            <Row>
                                <Col sm={{order:1,size: 12}}>
                                    <img className="logo_image" src={Logo} alt="Logo"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={{order:2,size:12}}>
                                <h1 className="display-1">Menze</h1>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm={{order:3}} md={{size:4}} style={{margin:"auto"}}>
                            <LoginForm cities={this.state.cities}/>
                        </Col>
                    </Row>
                    <Row>
                        <CityPicker cities={this.state.cities}/>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default connect(state => ({
    user: state.user
}))(StartPage)