import React, {Component} from 'react';
import {Button, Form, FormGroup, Input, Label, Alert} from "reactstrap";
import './LoginForm.css';
import RegisterForm from "../RegisterForm/RegisterForm";
import {isLoggedIn, login,} from "../../../Security/Security";
import {Redirect} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import {updateUser} from "../../../Util/ReduxStore";

export class LoginForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            redirect: "",
            user: null,
            error: ""
        }
    }

    handleUsernameChange = (event) => {
        this.setState({
            username : event.target.value
        })
    };

    handlePasswordChange = (event) => {
        this.setState({
            password : event.target.value
        })
    };

    handleLogin = () => {
        login(this.state.username, this.state.password, true).then(r => {
            if(isLoggedIn()) {
                updateUser()
                    .then(r => {
                        this.setState({
                            redirect: this.props.user.cityId,
                        })
                    })
            }
        }).catch(e => {
            this.setState({error:"Wrong email and/or password!"})
        })
    };

    render() {
        if(this.state.redirect !== "") {
            return <Redirect to={`/home/${this.state.redirect}`}
            />
        }

        return (
            <Form className="Login-form">
                <Alert style={{margin:10}} color="danger" isOpen={this.state.error !== ""}>
                    {this.state.error}
                </Alert>
                <FormGroup style={{margin:10}}>
                    <Label for="username">Email</Label>
                    <Input type="username" name="Username" id="uName" onChange={this.handleUsernameChange}
                           placeholder="Unesi email..."/>
                    <Label for="password">Lozinka</Label>
                    <Input type="password" name="Password" id="pWord" onChange={this.handlePasswordChange}
                           placeholder="Unesi lozinku"/>
                </FormGroup>
                <Button onClick={this.handleLogin}>Login</Button>
                <br/>
                <RegisterForm cities={this.props.cities} hidden={false}/>
            </Form>
        )

    };
}
export default connect(state => ({
    user: state.user
}))(LoginForm);