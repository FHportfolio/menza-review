import React, {Component} from 'react';
import {Card, CardFooter, CardImg, Col} from 'reactstrap';
import './CityPicker.css';
import placeholder from '../../../Images/350x150.png';
import {Redirect} from "react-router-dom";

export class CityPicker extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: -1,
            cities: []
        };
    }


    handleOnClick = (city) => {
        this.setState({
            redirect: city.id
        })
    };

    render() {
        if (this.state.redirect > 0) {
            return <Redirect to={`/home/${this.state.redirect}`}/>;
        }
        return (
            this.props.cities
                .map((city) =>
                    <Col key={city.name} className="CityCol">
                        <Card onClick={() => this.handleOnClick(city)} className="CityCard">
                            <CardImg top src={city.image !== null ? city.image : placeholder} style={{height: "100%"}}/>
                            <CardFooter className="text-center">
                                {city.name !== undefined || city.name !== null ? city.name : ""}
                            </CardFooter>
                        </Card>
                    </Col>
                )
        )
    };
}
export default CityPicker;