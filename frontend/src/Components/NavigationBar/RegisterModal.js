import React , {Component} from 'react';
import {Button, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";



export class RegisterModal extends Component {
    render() {
        return (
            <Modal isOpen={this.props.modal} toggle={this.props.toggle}>
                <ModalHeader toggle={this.props.toggle}>Register</ModalHeader>
                <ModalBody>
                    <Form className="Register-Form">
                        <FormGroup>
                            <Label for="username">New username</Label>
                            <Input type="uname" name="uname" onChange={this.handleUsernameChange} id="uname"
                                   placeholder="Enter username"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="password">Password</Label>
                            <Input type="password" name="password" onChange={this.handlePasswordChange}
                                   id="password" placeholder="Enter password"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="email">Email</Label>
                            <Input type="email" name="email" onChange={this.handleEmailChange} id="email"
                                   placeholder="Enter Email"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="cities">Select preferred city</Label>
                            <Input type="select" name="cities" onChange={this.handleCityChanged} id="cities">
                                <option>Osijek</option>
                                <option>Split</option>
                                <option>Zagreb</option>
                            </Input>
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary">Register</Button>{' '}
                    <Button color="primary">Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}