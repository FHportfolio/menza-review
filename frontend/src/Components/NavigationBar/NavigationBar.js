import React, {Component} from 'react';
import './NavigationBar.css';
import {Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink,} from "reactstrap";
import LoginModal from "./LoginModal";
import {RegisterModal} from "./RegisterModal";
import {getAuthorizationHeader, isLoggedIn, logout} from "../../Security/Security";
import ProfileModal from "./ProfileModal";
import connect from "react-redux/es/connect/connect";
import {updateUser} from "../../Util/ReduxStore";
import axios from 'axios';
import {unreadMessagesNumberURL} from "../../Util/paths";

export class NavigationBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            modalRegister: false,
            modalLogin: false,
            user: this.props.user,
            redirect: false,
            modalProfile:false,
            unreadMessagesNumber: 0
        }
    }

    componentDidMount() {
        updateUser();

        axios({
            url: unreadMessagesNumberURL,
            method: 'get',
            headers: getAuthorizationHeader()
        }).then(r=> this.setState({unreadMessagesNumber:r.data})).catch(e => {throw e});
    }

    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    };

    toggleModalLogin = () => {
        this.setState({
            modalLogin: !this.state.modalLogin
        })
    };

    toggleModalRegister = () => {
        this.props.history.push("/");
    };

    handleLogout = () => {
        logout();
        this.props.history.push("/");
    };

    toggleProfileModal = () => {
        this.setState( {
            modalProfile: !this.state.modalProfile
        })
    };


    render() {
        const admin = this.props.user ? this.props.user.admin : false;
        if (this.props.userContent) {
            if (!isLoggedIn()) {
                return (
                    <div className="NavigationBar">
                        <Navbar color="light" light expand="md">
                            <NavbarBrand href="/" style={{fontFamily: "Pacifico", color: "#1F93C6"}}>MENZE</NavbarBrand>
                            <NavbarToggler onClick={this.toggle}/>
                            <Collapse isOpen={this.state.isOpen} navbar>
                                <Nav className="ml-auto" navbar>
                                    <NavItem className="login-link">
                                        <NavLink onClick={this.toggleModalLogin}>Prijavi se</NavLink>
                                        <LoginModal history={this.props.history} modal={this.state.modalLogin} toggle={this.toggleModalLogin}/>
                                    </NavItem>
                                    <NavItem className="login-link">
                                        <NavLink onClick={this.toggleModalRegister}>Registriraj se</NavLink>
                                        <RegisterModal modal={this.state.modalRegister}
                                                       toggle={this.toggleModalRegister}/>
                                    </NavItem>
                                </Nav>
                            </Collapse>
                        </Navbar>
                    </div>
                )
            } else {
                return (
                    <div className="NavigationBar">
                        <Navbar light expand="md">
                            <NavbarBrand href="/" onClick={this.handle}
                                         style={{fontFamily: "Pacifico", color: "#1F93C6"}}>MENZE</NavbarBrand>
                            <NavbarToggler onClick={this.toggle}/>
                            <Collapse isOpen={this.state.isOpen} navbar>
                                <Nav className="ml-auto" navbar>
                                    {
                                        isLoggedIn() &&
                                        <NavItem>
                                            <NavLink style={{cursor: "pointer"}} onClick={this.toggleProfileModal}>Promijeni zadani grad</NavLink>
                                            <ProfileModal history={this.props.history} modal={this.state.modalProfile} toggle={this.toggleProfileModal}
                                                          user={this.props.user}/>
                                        </NavItem>
                                    }
                                    {
                                        isLoggedIn() &&
                                        <NavItem>
                                            <NavLink href="/messages">Poruke {this.state.unreadMessagesNumber === 0 ? null: '(' + this.state.unreadMessagesNumber + ')'}</NavLink>
                                        </NavItem>
                                    }
                                    {admin &&
                                    <NavItem>
                                        <NavLink href="/admin">Admin Panel</NavLink>
                                    </NavItem>
                                    }
                                    <NavItem>
                                        <NavLink href="" onClick={() => this.handleLogout()}>Odjavi se</NavLink>
                                    </NavItem>
                                </Nav>
                            </Collapse>
                        </Navbar>
                    </div>
                )
            }
        } else {
            return (
                <div className="NavigationBar">
                    <Navbar light expand="md">
                        <NavbarBrand href="/" style={{fontFamily: "Pacifico", color: "#1F93C6"}}>MENZE</NavbarBrand>
                        <NavbarToggler onClick={this.toggle}/>
                        <Collapse isOpen={this.state.isOpen} navbar>
                            <Nav className="ml-auto" navbar>
                                <NavItem>
                                    <NavLink href={`/restaurant/${this.props.id}`}>{this.props.name}</NavLink>
                                </NavItem>
                                {!this.props.isMenu &&
                                <NavItem>
                                    <NavLink href={`/menu/daily/${this.props.id}`}>Menu</NavLink>
                                </NavItem>}
                            </Nav>
                        </Collapse>
                    </Navbar>
                </div>
            )
        }


    }
}
export default connect(state => ({
    user: state.user
}))(NavigationBar)