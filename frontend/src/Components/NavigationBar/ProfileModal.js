import React, {Component} from 'react';
import axios from 'axios';
import {Alert, Button, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {allCitiesURL, defaultCityURL} from "../../Util/paths";
import {getAuthorizationHeader} from "../../Security/Security";
import {updateUser} from "../../Util/ReduxStore";


export class ProfileModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cities: [],
            id: 1,
            user: this.props.user,
            visible: false,
        }
    }

    componentDidMount() {
        axios.get(allCitiesURL)
            .then(res => {
                this.setState({
                    cities: res.data
                })
            })
    }

    handleCityChanged = (event) => {
        let value = event.target.value;
        this.setState({
            id: value
        });
    };

    alertTimeout = () => {
        this.setState({visible: false});
        this.props.history.push("/home/" + this.state.id);
        window.location.reload();
    };

    handleChange = () => {
        axios({
            url: defaultCityURL(this.state.id),
            method: 'put',
            headers: getAuthorizationHeader()
        }).then(() => {
            this.setState({visible: true},
                () => setTimeout(() => this.alertTimeout(), 1000));
            updateUser();
        }).catch(e => console.log(e.response));
    };

    render() {
        return (
            <Modal toggle={this.props.toggle} isOpen={this.props.modal}>
                <ModalHeader toggle={this.props.toggle}>My Profile</ModalHeader>
                <ModalBody>
                    <Form className="Profile">
                        <FormGroup>
                            <Label for="cities">select preferred city</Label>
                            <Input type="select" name="cities" onChange={this.handleCityChanged} id="cities">
                                {this.state.cities !== undefined ? this.state.cities.map(city =>
                                    <option key={city.id} value={city.id}>{city.name}</option>) : null
                                }
                            </Input>
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter><Button onClick={this.handleChange} color="primary">Change</Button></ModalFooter>
                <Alert isOpen={this.state.visible} color="info">Preference saved, redirecting...</Alert>
            </Modal>
        )
    }
};

export default ProfileModal;