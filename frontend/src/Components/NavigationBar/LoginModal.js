import {Alert, Button, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import React, {Component} from 'react';
import {isLoggedIn, login} from "../../Security/Security";
import {updateUser} from "../../Util/ReduxStore";
import connect from "react-redux/es/connect/connect";

export class LoginModal extends Component{

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            redirect: "",
            error: ""
        }
    }

    handleUsernameChange = (event) => {
        this.setState({
            username : event.target.value
        })
    };


    handlePasswordChange = (event) => {
        this.setState({
            password : event.target.value
        })
    };

    handleLogin = () => {
        login(this.state.username, this.state.password, true).then(r => {
            if(isLoggedIn()) {
                updateUser()
                    .then(r => {
                        this.props.history.push("/");
                    })
            }
        }).catch(e => {this.setState({error:"Krivi email i/ili lozinka!"})})
    };

    render() {
        return (
            <Modal toggle={this.props.toggle} isOpen={this.props.modal}>
                <ModalHeader toggle={this.props.toggle}>Login</ModalHeader>
                <ModalBody>
                    <Alert style={{margin:10}} color="danger" isOpen={this.state.error !== ""}>
                        {this.state.error}
                    </Alert>
                    <Form className="Lform">
                        <FormGroup>
                            <Label for="username">Email</Label>
                            <Input type="username" name="Username" id="uName" onChange={this.handleUsernameChange}
                                   placeholder="Unesi email.."/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="password">Lozinka</Label>
                            <Input type="password" name="Password" id="pWord" onChange={this.handlePasswordChange}
                                   placeholder="Unesi lozinku.."/>
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter><Button onClick={this.handleLogin} color="primary">Prijavi se</Button></ModalFooter>
            </Modal>
        )
    }
}

export default connect(state => ({
    user: state.user
}))(LoginModal)