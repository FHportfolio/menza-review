import React, {Component} from 'react';
import NavigationBar from '../NavigationBar/NavigationBar';
import RestaurantList from "./RestaurantList";
import RestaurantMap from "./RestaurantMap";
import {Redirect} from "react-router-dom";
import {isLoggedIn} from "../../Security/Security";
import connect from "react-redux/es/connect/connect";
import {updateUser} from "../../Util/ReduxStore";
import axios from "axios";
import {menzaByCityIdURL} from "../../Util/paths";

export class HomePage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            collapse: false,
            position: [],
            restaurantChoice: {id: -1},
            user: null,
            restaurants:[]
        }
    }

     componentWillMount() {
         axios.get(menzaByCityIdURL(this.props.match.params.id))
             .then(r => {
                 this.setState({restaurants:r.data});
             }).catch(e => {throw e});
        if (isLoggedIn()) {
            updateUser();
        }
    }

    toggle = () => {
        this.setState({
            collapse: !this.state.collapse
        })
    };

    handleRestaurantChoice = (restaurant) => {
        this.setState({
            restaurantChoice: restaurant
        })
    };



    render() {
        if(this.state.restaurantChoice.id !== -1) {
           return  <Redirect to={`/restaurant/${this.state.restaurantChoice.id}`}/>
        }
        return (
            <div className="HomePage">
                <NavigationBar
                               userContent = {true}
                               history={this.props.history}
                />
                <RestaurantList toggle={this.toggle}
                                collapse={this.state.collapse}
                                restaurants={this.state.restaurants}
                                handleRes={this.handleRestaurantChoice}
                />
                <RestaurantMap toggle={this.toggle}
                               collapse={!this.state.collapse}
                               restaurants={this.state.restaurants}
                               height={window.innerHeight}
                               handleRes={this.handleRestaurantChoice}

                />
            </div>
        )
    }
}

export default connect(state => ({
    user: state.user,
}))(HomePage)