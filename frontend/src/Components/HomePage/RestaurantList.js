import React from 'react';
import {Button, Collapse, Row} from "reactstrap";

export const ReastaurantList = (props) => {

    let restaurants = props.restaurants.map(r =>
        <Row key={r.id}><Button color="secondary" block onClick={() => props.handleRes(r)}>{r.name}</Button></Row>);
    return (
        <div>
            <Button color="primary" block onClick={props.toggle}>Lista menzi</Button>
            <Collapse isOpen={props.collapse}>
                {restaurants}
            </Collapse>
        </div>
    )
};

export default ReastaurantList;