import React from 'react';
import {Button, Collapse} from "reactstrap";
import {Map, Marker, TileLayer, Tooltip} from 'react-leaflet';
import {calculatePosition} from "../../Util/Util";

export const RestaurantMap = (props) => {
    let restaurants = props.restaurants.map(r =>
        <Marker key={r.id} leaflet={true} position={[r.latitude,r.longitude]} onClick={() => props.handleRes(r)}>
            <Tooltip leaflet={true}>Go to {r.name}</Tooltip>
        </Marker>);
    let initialPosition = calculatePosition(props.restaurants);
    return (
        <div>
            <Button color="primary" block onClick={props.toggle}>Karta menzi</Button>
            <Collapse isOpen={props.collapse}>
                <div>
                    <Map center={initialPosition} zoom={13} style={{height:props.height}}>
                        <TileLayer leaflet={true}
                            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        />
                        {restaurants}
                    </Map>
                </div>
            </Collapse>
        </div>
    )
};

export default RestaurantMap;