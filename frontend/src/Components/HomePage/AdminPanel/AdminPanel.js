import React, {Component} from 'react'
import axios from 'axios'
import {
    Button,
    Card,
    CardFooter,
    CardHeader,
    CardImg,
    CardText,
    Col,
    Container,
    FormGroup,
    Input,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from "reactstrap";
import NavigationBar from "../../NavigationBar/NavigationBar";
import {imageApprovalURL, imageByImageId, pictureURL, sendMessage} from "../../../Util/paths";
import {_request, getAuthorizationHeader} from "../../../Security/Security";


export class AdminPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            images: [],
            showModal: false
        }
    }

    handleApproval = (id,approval) => {
        if (!approval){
            this.setState({showModal:true});
            this.setState({currentImage: id});
            return ;
        }
      axios({
          url: imageApprovalURL(id,approval),
          method:'post',
          data: null,
          headers:getAuthorizationHeader()
      }).then(() => {
          const newState = this.state;
          const index = newState.images.findIndex(item => item.id === id);
          newState.images.splice(index,1);
          this.setState(newState);
          window.location.reload()
          });
    };

    getImages = () => {
        _request(pictureURL, 'GET', null, getAuthorizationHeader())
            .then(res => {
                let images = res.data.sort((item1, item2) => item2.timestamp < item1.timestamp ? -1 : item2.timestamp > item1.timestamp ? 1 : 0).map(image => {
                    let date = new Date(image.timestamp);
                    return (
                        <div key={image.id} style={{marginTop:10}}>
                            <Col>
                                <Card>
                                    <CardHeader>User {image.username} posted on {date.toUTCString()}</CardHeader>
                                    <CardImg src={imageByImageId(image.id)}/>
                                    <CardText>{image.description === null ? "" : image.description}</CardText>
                                    <CardFooter className="text-center">
                                        <div style={{display:'flex', flexDirection:'row', justifyContent:'space-around'}}>
                                            <Button onClick={() =>this.handleApproval(image.id,true)} color="info">Approve</Button>
                                            <Button onClick={() =>this.handleApproval(image.id,false)} color="info">Delete</Button>
                                        </div>
                                    </CardFooter>
                                </Card>
                            </Col>
                        </div>
                    )
                });
                this.setState({images: images});
                return true;
            })

    };

    componentDidMount() {
        this.getImages();

    }

    handleRejectMessage = (event) => {
        this.setState({rejectionMessage: event.target.value});
    };

    toggle = () => {
        this.setState({showModal: !this.state.showModal});
    };

    handleRejectImage = () => {
        let messageDto = {
            text: this.state.rejectionMessage
        };
        axios({
            url: sendMessage(this.state.currentImage),
            method: 'post',
            data: messageDto,
            headers: getAuthorizationHeader()
        }).then(() => {
            axios({
                url: imageApprovalURL(this.state.currentImage,false),
                method:'post',
                data: null,
                headers:getAuthorizationHeader()
            }).then(r => {
                const newState = this.state;
                const index = newState.images.findIndex(item => item.id === this.state.currentImage);
                newState.images.splice(index,1);
                this.setState(newState);
                window.location.reload()
            })
        })
    };

    render() {
        return (
            <div>
                <Modal toggle={this.toggle} isOpen={this.state.showModal}>
                    <ModalHeader>Zašto je slika odbijena?</ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Input type="textarea" name="explanationText" onChange={this.handleRejectMessage} id="uname"
                                   placeholder="Unesi poruku..."/>
                        </FormGroup>
                        <Button onClick={this.handleRejectImage}>Send</Button>
                    </ModalBody>
                </Modal>
                <NavigationBar userContent={true} history={this.props.history}/>
                <Container>
                    <Col>
                        <Row>
                            <h1>Pending images</h1>
                        </Row>
                        {this.state.images}
                    </Col>
                </Container>
            </div>
        )
    }

}

export default AdminPanel;