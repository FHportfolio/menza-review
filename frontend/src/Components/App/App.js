import React, {Component} from 'react';
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom';
import StartPage from '../StartPage/StartPage';
import HomePage from '../HomePage/HomePage';
import './App.css';
import {RestaurantDetails} from "../MenzaDetails/RestaurantDetails";
import MenuNavbar from "../Menu/MenuNavbar/MenuNavbar";
import {Provider} from "react-redux";
import {createStore} from "redux";
import rootReducer from "../../Util/ReduxStore";
import MessagesMain from "../Messages/MessagesMain";
import AdminPanel from "../HomePage/AdminPanel/AdminPanel";
import {getAuthorities, isLoggedIn} from "../../Security/Security";

export const store = createStore(rootReducer);


class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <div className="App">
                    <BrowserRouter>
                        {/*<main>*/}
                        <div>
                            <Switch>
                                <Route exact path='/' component={StartPage} name='start'/>
                                <Route path='/home/:id?' component={HomePage} name='home'/>
                                <Route path='/restaurant/:id?' component={RestaurantDetails} name='details'/>
                                <Route path='/menu/daily/:id?' component={MenuNavbar} name='menu'/>
                                <AdminRoute path='/admin/' component={AdminPanel} name='admin'/>
                                <LoggedInRoute path='/messages' component={MessagesMain} name='MessagesMain' />
                                <Route component={Error} name='error'/>
                                <Route component={StartPage} />
                            </Switch>
                            <footer className="fixed-bottom">
                              <span className="float-right" style={{marginRight: "10px", color: "#1F93C6", height:"20px"}}>
                              v0.0.1  Sarmice 18/19
                              </span>
                            </footer>
                        {/*</main>*/}
                        </div>
                    </BrowserRouter>
                </div>
            </Provider>
        );
    }
}

const AdminRoute = ({component: Component, ...rest}) => {
    return (
        <Route
            {...rest}
            render={props => {
            return (
                getAuthorities() === "ADMIN" ?
                    <Component {...props}/> :
                    <Redirect to="/" />
            )}}
        />
    );
};

const LoggedInRoute = ({component: Component, ...rest}) => {
    return (
        <Route
            {...rest}
            render={props => isLoggedIn() ? <Component {...props}/> : <Redirect to="/" />} />
    )
};

export const Error = () => {
    return (
        <div className="PlaceHolder 3">
            <h1>Ooops something went wrong</h1>
        </div>
    )
};
export default App;
