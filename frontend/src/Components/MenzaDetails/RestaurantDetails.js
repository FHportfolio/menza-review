import React, {Component} from 'react';
import axios from 'axios';
import NavigationBar from "../NavigationBar/NavigationBar";
import {menzaByIdURL} from "../../Util/paths";
import {Button, Card, CardBody, CardFooter, CardHeader, CardImg, Col, Container, Row} from "reactstrap";
import './RestaurantDetails.css'
import {Redirect} from "react-router-dom";
import {RESET_COMMENTS, RESET_RESTAURANT_MEALS} from "../../Util/ReduxStore";
import {store} from "../App/App";

export class RestaurantDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            restaurant: {},
            redirect: false,
            loading: "Loading"
        }
    }

    handleOnClick = () => {
        this.setState({
            redirect: true,
        },() => setTimeout(this.alertTimeout,1000))
    };


    componentWillMount() {
        store.dispatch({type: RESET_COMMENTS});
        store.dispatch({type: RESET_RESTAURANT_MEALS});
        axios.get(menzaByIdURL(this.props.match.params.id))
            .then(res => {
                this.setState({
                    restaurant: res.data
                })
            })
    }

    render() {
        let all = this.state.restaurant.info !== undefined ? this.state.restaurant.info : "Mock|Data|Opp|Other";
        let allInfo = all.split("|");
        let description = allInfo[0];
        let address = allInfo[1];
        let number = allInfo[2];
        let other = allInfo[3];
        if (this.state.redirect) {
            return <Redirect to={`/menu/daily/${this.state.restaurant.id}`}/>;
        }
        else {
            return (
                <div className="RestaurantDetails">
                    <NavigationBar name={this.state.restaurant.name}
                                   id={this.state.restaurant.id}
                                   userContent={true}
                                   history={this.props.history}

                    />
                    <Container>
                        <div style={{marginTop:10, display:'flex', flexDirection:'column'}}>
                            <div style={{backgroundColor:"#f5f5f5", display:'flex', flexDirection:'column'}}>
                                <Button outline color="primary" onClick={ () => {this.props.history.push("/menu/daily/" + this.state.restaurant.id)}} >Menu</Button>
                            </div>
                            <Row>
                                <Col sm="12" md="6">
                                    <Row>
                                        <Col sm={{order: 1, size: 12}} md={{size: 12}}>
                                            <div className="imageHolder">
                                                <Card>
                                                    <CardImg width="100%" src="https://via.placeholder.com/600x400"
                                                             alt="An image of a restaurant"/>
                                                </Card>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col sm={{order: 2, size: 12}} md={{size: 12}}>
                                            <Card className="text-center info">
                                                <CardHeader>
                                                    Info
                                                </CardHeader>
                                                <CardBody>
                                                    {address}
                                                    <br/>
                                                    {number}
                                                    <br/>
                                                    {other}
                                                </CardBody>
                                                <CardFooter/>
                                            </Card>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col sm={{order: 3, size: 12}} md="6">
                                    <Card className="descriptionHolder">
                                        <CardHeader>Opis</CardHeader>
                                        <CardBody>
                                            {description}
                                        </CardBody>
                                        <CardFooter/>
                                    </Card>
                                </Col>
                            </Row>
                        </div>
                    </Container>
                </div>)
        }
    }
}

export default RestaurantDetails;