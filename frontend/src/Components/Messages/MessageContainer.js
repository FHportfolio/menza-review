import React from 'react';
import {Card, CardFooter, CardHeader, CardText} from "reactstrap";
import axios from "axios";
import {readMessageById} from "../../Util/paths";
import {getAuthorizationHeader} from "../../Security/Security";

export default class MessageContainer extends React.Component {

    state = {
        visible: false
    };

    openMessage = () =>{
        if (!this.props.message.read) {
            axios({
                url: readMessageById(this.props.message.id),
                method: 'post',
                headers: getAuthorizationHeader()
            }).then(r => {
                this.props.markAsRead();
                this.setState({visible: true})
            }).catch(e => {
                throw e
            })
        }
        else {
            this.setState({visible: !this.state.visible});
        }

    };

    render() {
        return (
            <div onClick={this.openMessage} style={{...{margin:10, paddingLeft:10}, backgroundColor: this.props.message.read ? '' : 'green'}}>
                <Card>
                    <CardHeader>Korisnik {this.props.message.username} kaže</CardHeader>
                    {this.state.visible ? <CardText>{this.props.message.text}</CardText> :<div/> }
                    <CardFooter>{(new Date(this.props.message.timestamp)).toUTCString()}</CardFooter>
                </Card>
            </div>
        )
    }
}