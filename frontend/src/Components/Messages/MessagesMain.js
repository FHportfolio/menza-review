import React from 'react';
import {getAuthorizationHeader} from "../../Security/Security";
import {getMessagesURL} from "../../Util/paths";
import axios from 'axios';
import MessageContainer from "./MessageContainer";
import NavigationBar from "../NavigationBar/NavigationBar";
import connect from "react-redux/es/connect/connect";
import {Label} from "reactstrap";

class MessagesMain extends React.Component {

    constructor(props){
        super(props);

        this.state ={
            messages: []
        }
    }

    markAsRead(index){
        let list = new Array(this.state.messages);
        list[0][index].read = true;
        this.setState({
            messages: list[0]
        })
    };

    componentDidMount() {
        this.getMessages();
    }


    getMessages = () => {
        axios({
            url: getMessagesURL,
            method:'get',
            headers: getAuthorizationHeader()
        }).then(r => {
            this.setState({messages: r.data})
        }).catch(e => {throw e})
    };

    render() {
        return (
            <div>
                <NavigationBar
                                userContent = {true}
                                history={this.props.history}
                />
                {
                <div>
                    <div style={{margin:25}}>
                        <Label>Ukupno poruka: ({this.state.messages.length})</Label>
                    </div>
                    {[...this.state.messages].map((message, i) => {
                        return <MessageContainer key={message.id} index={i} message={message} getMessages={this.getMessages} markAsRead={() => this.markAsRead(i)}/>
                    })}
                </div>
                }
            </div>
        )
    }
}

export default connect(state => ({
    user: state.user
}))(MessagesMain)