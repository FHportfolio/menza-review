export function calculatePosition(arr) {

    let position = [];
    let latitude = 0;
    let longitude = 0;

    arr.forEach(r => {
        latitude = latitude + r.latitude / arr.length;
        longitude = longitude + r.longitude / arr.length;
    });

    position.push(latitude);
    position.push(longitude);

    return position

}

export const JWT_TOKEN = "jwt_token";
export const ADMIN = "ADMIN";
export const USER = "USER";
export const NOT_LOGGED_IN = "ANON";

export const lorem = "Lorem ipsum dolor sit amet, nibh doming facilis cu cum. Platonem oportere has cu, civibus noluisse erroribus qui ad. Nec tincidunt elaboraret concludaturque in, eum cu purto case elit. Id agam inimicus mea, aperiam appetere ne eos. Eu sit perpetua necessitatibus, no vis omnis vitae, in purto expetendis consetetur vim. Est ut soleat alienum. Omnes senserit ut mel, mea ad eleifend patrioque, ad solet euripidis repudiandae pri.\n" +
    "\n" +
    "Putant viderer virtute vel at, mea quis nusquam platonem ex. Has ea insolens intellegam, graeci iriure ex est. Te adipisci comprehensam sea, dolorum pericula pro at. Has omnis eleifend salutatus ea. Cum sumo principes no, at option torquatos sit.\n" +
    "\n" +
    "Pro lorem probatus pertinax ad, ei graecis consectetuer vis. Eam tantas luptatum consulatu ea. Unum percipit at eos, cu vel purto virtute sanctus. No rebum principes quo, probo intellegebat delicatissimi ei per. Nec admodum maluisset cu. In oratio appareat iudicabit sed.\n" +
    "\n" +
    "His mutat soleat patrioque in, diam noluisse scriptorem no quo. Tempor recteque ullamcorper has et, harum mnesarchum consequuntur sed id, ut possim melius molestiae per. Vim altera legimus accusata et, eum ne viris eligendi comprehensam. Eirmod facilisis sententiae nec ex, inermis deterruisset et pri. Omnis partiendo maiestatis mel ut.";

