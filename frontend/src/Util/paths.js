function hostPath() {
    switch (process.env.NODE_ENV) {
        case "development":
            return "http://localhost:8080";
        case "production":
            return window.location.protocol + "//" + process.env.REACT_APP_BACKEND;
        default:
            return "error";
    }

}


export const allCitiesURL = hostPath() + "/cities";
export const loginVerifierURL = hostPath() + "/oauth/token";
export const currentUserURL = hostPath() + "/user";
export const pictureURL = hostPath() + "/picture";
export const pendingURL = hostPath() + "/pending";
export const ratingURL = hostPath() + "/rating";
export const commentsURL = hostPath() + "/comment";
export const registerURL = hostPath() + "/register";
export const unreadMessagesNumberURL = hostPath() + "/messages/unread";
export function sendMessage(id) {return  hostPath() + `/message/${id}`}
export function defaultCityURL(id) {return hostPath() + `/defaultCity?id=${id}`; }
export function imageApprovalURL(id,approved) {return hostPath() + `/picture/approve/${id}/${approved}`};
export const getMessagesURL = hostPath() + "/messages";
export function ratingMyRatingByMenzaMealId(id) { return hostPath() + `/menzameal/${id}/myrating`}
export function menzaByCityIdURL(id) {return  hostPath() + `/city/${id}/menze`}
export function menzaByIdURL(id) {return hostPath() + `/menza?id=${id}`}
export function dailyOfferByMenzaId(id) {return hostPath() + `/menza/${id}/dailyoffer`}
export function menzaMealById(id) { return hostPath() + `/menzameal?id=${id}`}
export function imageByImageId(id) { return hostPath() + `/img/src/${id}`;}
export function commentByMenzaMealId(id) { return hostPath() + `/menzameal/${id}/comments`}
export function imagesByMenzaMealId(id) {return hostPath() + `/menzameal/${id}/images`}
export function authorizationURL() { return hostPath() + "/oauth/token"}
export function avgratingURL(id) { return hostPath() + `/menzameal/${id}/rating`}
export function readMessageById(id) {return hostPath() + `/message/${id}/read`}