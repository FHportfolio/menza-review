import axios from "axios";
import {commentByMenzaMealId, currentUserURL} from "./paths";
import {getAuthorizationHeader} from "../Security/Security";
import {store} from "../Components/App/App";

const initialState = {
    allCities: [],
    restaurants: [],
    offers: [],
    user: null,
    restaurantMeals: [],
    allComments: [],
    test: []
};

export default function rootReducer(state = initialState, action) {
    switch (action.type) {
        case ALL_CITIES:
            return {...state, ...{allCities: action.payload}};
        case OFFERS:
            return {...state, ...{offers: action.payload}};
        case RESTAURANT:
            return {...state, ...{restaurants: action.payload}};
        case USER:
            return {...state, ...{user: action.payload}};
        case RESTAURANT_MEALS:
            return {...state, ...{restaurantMeals: state.restaurantMeals.concat(action.payload)}};
        case ADD_TO_ALL_COMMENTS:
            return {...state, ...{allComments: state.allComments.concat(action.payload)}};
        case "test":
            return {...state, ...{test: state.test.concat(action.payload)}};
        case SELECTED_TYPE:
            return {...state, selected:action.payload};
        case RESET_COMMENTS:
            return {...state, ...{allComments:[]}};
        case RESET_RESTAURANT_MEALS:
            return {...state, ...{restaurantMeals: []}};
        default:
            return state;
    }
}

export const updateUser = async () => {
    return axios({
        url: currentUserURL,
        method: 'get',
        headers: {...getAuthorizationHeader(), 'Access-Control-Request-Origin': '*'}
    }).then(r => {
            store.dispatch({type: USER, payload: r.data});
            return r;
        })
        .catch(e => {
            throw e
        });
};

export const getComments = async (restaurantMeals) => {
    restaurantMeals = Array.from(new Set(restaurantMeals));
    restaurantMeals.forEach(meal => {
        axios.get(commentByMenzaMealId(meal.menzaMealId))
            .then(res => {
                store.dispatch({type: ADD_TO_ALL_COMMENTS, payload: res.data});
            })
    });
};

export const ALL_CITIES = "ALL_CITIES";
export const OFFERS = "OFFERS";
export const RESTAURANT = "RESTAURANT";
export const USER = "USER";
export const RESTAURANT_MEALS = "RESTAURANT_MEALS";
export const ADD_TO_ALL_COMMENTS = "ADD_TO_ALL_COMMENTS";
export const SELECTED_TYPE = "SELECTED_TYPE";
export const RESET_COMMENTS = "RESET_COMMENTS";
export const RESET_RESTAURANT_MEALS = "RESET_RESTAURANT_MEALS";