import {JWT_TOKEN, NOT_LOGGED_IN} from "../Util/Util";
import queryString from 'qs';
import {authorizationURL} from '../Util/paths';
import axios from "axios";
import jwtDecode from "jwt-decode";

export const getToken = () => {
    return JSON.parse(localStorage.getItem(JWT_TOKEN));
};

export const isLoggedIn = () => {
    return getToken() !== null && getToken() !== undefined;
};

export const getAuthorities = () => {
    if (!isLoggedIn()) {
        return NOT_LOGGED_IN;
    }
    return jwtDecode(getToken().access_token).authorities[0];
};

export const saveToken = (token) => {
    localStorage.setItem(JWT_TOKEN, token);
};

export const removeToken = () => {
    localStorage.removeItem(JWT_TOKEN);
};

export const getAuthorizationHeader = () => {
    let token = getToken();
    if (token !== null && token !== undefined) {
        return {
            "Authorization": "Bearer " + token.access_token
        };
    }
    else {
        // default client and secret (client_id:secret)
        return {"Authorization": "Basic Y2xpZW50X2lkOnNlY3JldA=="};
    }
};

export const login = async (username, password, rememberMe) => {
    if (isLoggedIn()) {
        let data = {
            grant_type:"refresh_token",
            refresh_token: getToken().refresh_token
        };
        return axios({
            url:authorizationURL(),
            method:'post',
            data: queryString.stringify(data),
            headers: {Authorization: "Basic Y2xpZW50X2lkOnNlY3JldA==", 'Access-Control-Request-Origin': '*'}
        }).then(r => {
            saveToken(JSON.stringify({...r.data, refresh_token:data.refresh_token}));
            return true;
        }).catch(e => {throw e});
    }
    else {
        return axios({
            url:authorizationURL(),
            method:'post',
            data: queryString.stringify({
                username:username,
                password: password,
                grant_type:'password'
            }),
            headers:getAuthorizationHeader()
        }).then(r => {
            saveToken(JSON.stringify(r.data));
            return true;
        }).catch(e => {throw e});
    }
};

export const logout = () => {
    removeToken();
};

export const _request = async (url, method, data, headers) =>{
    return axios({
        url:url,
        method:method,
        data:data,
        headers:headers
    }).then(r => r)
        .catch(e => {
            login().then(r => {
                return axios({
                    url:url,
                    method:method,
                    data:data,
                    headers:headers
                }).then(r => r)
                    .catch(e => {throw e})
            })
                .catch(e => {throw e})
        });
};
